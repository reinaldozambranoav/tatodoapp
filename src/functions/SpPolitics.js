export const fnGetIsPoliticAvailable = (defaultValue, index, id, data, extraValidation) => {
  if (index === -1) {
      return (extraValidation == null) ? defaultValue : extraValidation;
  }
  try {
      const isActive = data[index].key.find(x => +x.id === +id).active;
      return (extraValidation == null) ? isActive : (isActive && extraValidation);
  } catch (error) {
      return (extraValidation == null) ? defaultValue : extraValidation;
  }
}

export const fnGetIsSubPoliticAvailable = (defaultValue, id, data, extraValidation) => {
  try {
      const isActive = data.find(x => +x.id === +id).active;
      return (extraValidation == null) ? isActive : (isActive && extraValidation);
  } catch (error) {
      return (extraValidation == null) ? defaultValue : extraValidation;
  }
}