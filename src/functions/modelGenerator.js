import fnGetTypeOfElement from "helper/fnGetTypeOfElement"

const fnGenerateModel = (data, JSONoutput = false, innerKeyName = '') => {
  let newModel = new Object()
  for (const key in data) {
    let value
    switch (fnGetTypeOfElement(data[key])) {
      case 'string':
        value = innerKeyName !== '' ? `data?.${innerKeyName}?.${key} ?? ''` : `data?.${key} ?? ''`
        break
      case 'number':
        value = innerKeyName !== '' ? `data?.${innerKeyName}?.${key} ?? 0` : `data?.${key} ?? 0`
        break
      case 'array':
        data[key].forEach(x => {
          console.log(x)
        })
        // PARA FUTURO AGREGAR RETURN DE data.key.map para casos donde los array vengan con un objeto de refefencia
        // value = innerKeyName !== ''
        //   ? `data?.${innerKeyName}?.${data[key].map((x, i) => { if (i === 0) return ({ ...fnGenerateModel(x) }) })} ?? []`
        //   : `data?.${data[key].map((x, i) => { if (i === 0) return ({ ...fnGenerateModel(x) }) })} ?? []`
        value = innerKeyName !== '' ? `data?.${innerKeyName}?.${key} ?? []` : `data?.${key} ?? []`
        break
      case 'object':
        value = fnGenerateModel(data[key], false, key)
        break
      case 'boolean':
        value = innerKeyName !== '' ? `data?.${innerKeyName}?.${key} ?? false` : `data?.${key} ?? false`
        break
      case 'undefined':
        value = innerKeyName !== '' ? `data?.${innerKeyName}?.${key} ?? undefined` : `data?.${key} ?? undefined`
        break
      case 'null':
        value = innerKeyName !== '' ? `data?.${innerKeyName}?.${key} ?? null` : `data?.${key} ?? null`
        break
      default:
        return console.error(`El tipo de dato de la propiedad: ${key} del objeto no esta soportada por este modelo`)
    }
    newModel = ({ ...newModel, [key]: value })
  }
  return JSONoutput ? JSON.stringify(newModel) : newModel
}
export default fnGenerateModel