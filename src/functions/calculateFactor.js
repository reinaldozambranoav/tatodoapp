let currencyList = [], currencyId = 0;

export const setCurrencyList = (data) => currencyList = data
export const setCurrencyId = (id) => currencyId = id
export const calculateFactor = (value, customCurrency, precision = 4, inverseCalculation = false, useBase = false) => {
    let currency, resp
    if ((currencyId > 0 || customCurrency) && value && !useBase) {
        if (value > 0) {
            currency = customCurrency ?? currencyList.find(x => x.id === currencyId)
            resp = fnMathCalculation(value, currency?.isBase ? 1 : currency.factor, !inverseCalculation ? currency.typeCalc : currency.typeCalc === "*" ? "/" : "*", precision)
            return resp
        }
    }else if(useBase){
        currency = currencyList.find(x => x.isBase)
        resp = fnMathCalculation(value, 1, !inverseCalculation ? currency.typeCalc : currency.typeCalc === "*" ? "/" : "*", precision)
        return resp
    }
    return {
        amount: value ?? 0,
        amountLabel: (value ?? 0).formatNumb(2),
        amountNoFixed: (value ?? 0).toFixedNoRounding(precision),
        amountNoFixedLabel: (+(value ?? 0).toFixedNoRounding(precision)).formatNumb(2)
    }
}
export const newSelectedCurrency = () => {
    const indexCurrency = currencyList.findIndex(x => x.id === currencyId)
    const newIndex = indexCurrency + 1 > currencyList.length - 1 ? 0 : indexCurrency + 1
    setCurrencyId(currencyList[newIndex].id)
    return currencyList[newIndex]
}

export const fnMathCalculation = (value1, value2, operator, precision = 2) => {
    let response = 0
    let precision1 = 0;
    let precision2 = 0;
    try {
        try {
            precision1 = value1.toString().split(".")[1].length ?? 0
        } catch (error) {
            precision1 = 0
        }
        try {
            precision2 = value2.toString().split(".")[1].length ?? 0
        } catch (error) {
            precision2 = 0
        }
        const totalDecimals = precision1 >= precision2 ? precision1 : precision2
        const totalDecimalsMult = totalDecimals * 2
        const calculation = +("1".padEnd((totalDecimals + 1), "0"))
        const calculationMult = +("1".padEnd((totalDecimalsMult + 1), "0"))
        const newValue1 = (value1 * (calculation === 0 ? 1 : calculation)).round(totalDecimals)
        const newValue2 = (value2 * (calculation === 0 ? 1 : calculation)).round(totalDecimals)
        switch (operator) {
            case "+":
                response = (newValue1 + newValue2) / calculation
                break;
            case "-":
                response = (newValue1 - newValue2) / calculation
                break;
            case "*":
                response = (newValue1 * newValue2) / calculationMult
                break;
            case "/":
                response = (newValue1 / newValue2)
                break;
            default:
                response = newValue1 / calculation
        }
    } catch (error) {
        console.error('fnMathCalculation => ', error)
    }
    return {
        amount: +response.round(precision),
        amountLabel: (response.round(2)).formatNumb(2),
        amountNoFixed: +response.toFixedNoRounding(2),
        amountNoFixedLabel: (+response.toFixedNoRounding(2)).formatNumb(2)
    }
}

export const decimalToRight = (value) => {
    if(value.substring(value.length - 1, value.length) === "0"){
        value = value.substring(0, value.length - 1)
        return decimalToRight(value)
    }
    return value
}