import * as moment from 'moment'
import CryptoJS from "crypto-js"

import { getHashLogin, postHashLogin, removeHashLogin, } from 'services/localforage/HashLogin'
import { setResponseForage } from 'services/localforage/Localforage'


const hashKey = "%$!mpl!tP0ST0t4l4pP%"

export const GenerateHashLogin = async (ip, company, login, station, config, date = null, expDate = null, permisology = null) => {
  const request = {
    ip: ip,
    company: company,
    login: login,
    station: station,
    date: date === null ? moment(new Date()).format("YYYY-MM-DDTHH:mm:ss") : date,
    getDate: moment(new Date()).format("YYYY-MM-DDTHH:mm:ss"),
    expDate: expDate === null ? moment(new Date().addDays(station.daysAutoLogin)).format("YYYY-MM-DDTHH:mm:ss") : expDate,
    permisology: permisology,
    config: config,
  }
  const newHashLogin = CryptoJS.AES.encrypt(btoa(unescape(encodeURIComponent(JSON.stringify(request)))), hashKey)
  return postHashLogin(company.license, company.companyName, company.officeId, company.officeName, btoa(newHashLogin))
}

export const ReadHashLogin = () => {
  return new Promise(async (resolve) => {
    const result = await getHashLogin()
    if (result.error === false) {
      if (result.data.length === 1) {
        if (result.data[0].office.length === 1) {
          const newHashLoginDecrypt = CryptoJS.AES.decrypt(decodeURIComponent(escape(atob(result.data[0].key).toString())), hashKey)
          const newHashLoginBytes = newHashLoginDecrypt.toString(CryptoJS.enc.Utf8)
          const newHash = JSON.parse(atob(newHashLoginBytes))
          resolve(isValidHashLogin(result.data[0].lic, result.data[0].office[0].id, result.data[0].office[0].name, newHash))
        }
        resolve(setResponseForage(false, 'select', result.data))
      } else {
        resolve(setResponseForage(false, 'select', result.data))
      }

    } else {
      resolve(setResponseForage(false, '', null))
    }
  })
}

export const ReadUniqueHashLogin = (lic, officeId, officeName, key) => {
  return new Promise(async (resolve) => {
    const newHashLoginDecrypt = CryptoJS.AES.decrypt(key.toString(), hashKey)
    const newHashLoginBytes = newHashLoginDecrypt.toString(CryptoJS.enc.Utf8)
    const newHash = JSON.parse(atob(newHashLoginBytes))
    resolve(isValidHashLogin(lic, officeId, officeName, newHash))
  })
}

const isValidHashLogin = async (lic, officeId, officeName, data) => {
  if (moment(new Date()).format("YYYY-MM-DDTHH:mm:ss") < data.date ||
    moment(new Date()).format("YYYY-MM-DDTHH:mm:ss") < data.getDate) {
    await removeHashLogin(lic, officeId)
    return setResponseForage(true, 'Ha expirado su inicio de sesión por comportamiento inusual, ingrese nuevamente', [])
  } else if (moment(new Date()).format("YYYY-MM-DDTHH:mm:ss") > data.expDate) {
    await removeHashLogin(lic, officeId)
    return setResponseForage(true, 'Ha expirado su inicio de sesión, ingrese nuevamente', [])
  }
  else {
    data = {
      ...data,
      office: {
        id: officeId,
        name: officeName
      }
    }
    return setResponseForage(false, '', data)
  }
}

export const fnGenerateHash = (info) => btoa(CryptoJS.AES.encrypt(btoa(JSON.stringify(info)), hashKey))

export const fnReadHash = (info) => {
  const newHashLoginDecrypt = CryptoJS.AES.decrypt(atob(info).toString(), hashKey)
  const newHashLoginBytes = newHashLoginDecrypt.toString(CryptoJS.enc.Utf8)
  const newHash = JSON.parse(atob(newHashLoginBytes))

  return newHash
}