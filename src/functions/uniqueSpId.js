import moment from 'moment'
import { v4 as uuid } from 'uuid'
const fnUniqueSpId = () => {
  let sDate = String(moment(new Date()).format("YYYYMMDDHHmmss")),
    sRandom = String(Math.floor(Math.random() * (9999)));

  return `${uuid()}|${sDate}|${sRandom}`;
}

export default fnUniqueSpId