import { CompanyContext } from "contexts/Company/CompanyContext"
import { CurrencyContext } from "contexts/Currency/CurrencyContext"
import { IpContext } from "contexts/Ip/IpContext"
import { LanguageContext } from "contexts/Language/LanguageContext"
import { LoginContext } from "contexts/Login/LoginContext"
import { OfflineSyncContext } from "contexts/OfflineSync/OfflineSyncContext"
import { PermissionContext } from "contexts/Permissions/PermissionContext"
import { StationContext } from "contexts/Station/StationContext"
import { useContext, useState, useEffect } from "react"
import { dbSimplit, postLocalDB } from "services/localforage/Localforage"

const useDataPersistEmu = async () => {
  const { companyState, companyDispatch } = useContext(CompanyContext)
  const { loginState, loginDispatch } = useContext(LoginContext)
  const { currencyState, currencyDispatch } = useContext(CurrencyContext)
  const { ipState, ipDispatch } = useContext(IpContext)
  const { languageState, setLanguageState } = useContext(LanguageContext)
  const { offlineSyncState, offlineSyncDispatch } = useContext(OfflineSyncContext)
  const { permissionState, permissionDispatch } = useContext(PermissionContext)
  const { stationState, stationDispatch: { setStation } } = useContext(StationContext)
  const [ contextData ] = useState({
    companyState, loginState, currencyState,
    ipState, languageState, offlineSyncState,
    permissionState, stationState,
  })

  const isDev = process.env.NODE_ENV === 'development'
  useEffect(() => {
    let response
    const fnQueryLF = async () => {
      try {
        if (isDev) {
          if (stationState.name !== '') {
            for (const key in contextData) {
              response = await postLocalDB(key, contextData[ key ])
            }
          } else if (!loginState.isLogin && !stationState.name !== '') {
            for (const key in contextData) {
              response = await dbSimplit.getItem(key)
              console.log(key, '=>', response)
              switch (key) {
                case 'companyState':
                  companyDispatch.setCompanyState(response)
                  break;
                case 'loginState':
                  loginDispatch.setLoginState(response)
                  break;
                case 'currencyState':
                  currencyDispatch.setCurrencyState(response)
                  break;
                case 'ipState':
                  ipDispatch.setIpState(response)
                  break;
                case 'languageState':
                  setLanguageState(response)
                  break;
                case 'offlineSyncState':
                  offlineSyncDispatch._setOfflineSyncState(response)
                  break;
                case 'permissionState':
                  permissionDispatch.setPermissionState(response)
                  break;
                case 'stationState':
                  console.log('stationState => ', response)
                  setStation(response)
                  break;
                default:
                  console.log("this key doen't match with the context state keys => ", key)
                  break;
              }
            }
          }
        }
      } catch (error) {
        console.error('useDataPersistEmu => ', error)
      }
    }
    fnQueryLF()
  }, [ stationState ])
}

export default useDataPersistEmu