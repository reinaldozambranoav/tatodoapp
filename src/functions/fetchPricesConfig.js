export const fnFetchPricesConfig = () => fetch(process.env.NODE_ENV === 'development' ? 'config/prices.json' :
    'https://farmahorro.simplitpos.com/config/prices.json')
    .then((r) => r.json())
    .then((data) => data)