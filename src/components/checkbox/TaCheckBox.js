import React, { forwardRef, memo } from 'react'
import { FormControlLabel, Checkbox } from '@mui/material'
import { Controller } from 'react-hook-form';
import PropTypes from 'prop-types'
import TaTypography from '../typography/TaTypography';
import { makeStyles } from '@mui/styles';

const checkboxStyles = makeStyles({
  root: {
    color: 'var(--main-table-color0)',
    '&$checked': {
      color: 'var(--main-table-color0)',
    },
    margin: '2px',
    padding: '2px',
  },
  checked: {},
})

const TaCheckBox = memo(forwardRef(({ id, name, control, status, disabled, label, onChange, uniqueKey, rules }, ref) => {

  const classes = checkboxStyles()

  return (
    <>
      {control ? <Controller
        name={name}
        control={control}
        rules={rules}
        shouldUnregister={true}
        render={({ field }) => {
          return (
            <FormControlLabel
              control={<Checkbox
                id={id}
                name={field.name}
                onChange={field.onChange}
                inputRef={field.ref}
                checked={field.value}
                disabled={disabled}
                classes={{ root: classes.root, checked: classes.checked }}
              />}
              label={<TaTypography>{label}</TaTypography>} />)
        }} />
        : <FormControlLabel
          control={<Checkbox
            id={id}
            name={name}
            onChange={onChange}
            checked={status}
            inputRef={ref}
            disabled={disabled}
            classes={{ root: classes.root, checked: classes.checked }}
          />}
          label={<TaTypography>{label}</TaTypography>} />
      }
    </>
  )
}))

TaCheckBox.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  control: PropTypes.object,
  label: PropTypes.string,
  disabled: PropTypes.bool,
  uniqueKey: PropTypes.string,
}

TaCheckBox.defaultProps = {
  status: false,
  disabled: false,
  label: '',
}

export default TaCheckBox
