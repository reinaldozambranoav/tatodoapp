import TaCheckBox from './TaCheckBox'
import { create } from 'react-test-renderer'

test('', () => {
  const component = create(<TaCheckBox name={'soy el nombre'} label={'soy el label'} />)
  let tree = component.toJSON();
  let label
  let name = tree.children[0].children[0].props.name
  if (tree.children[1].children[0].children === null) {
    label = ''
  } else {
    label = tree.children[1].children[0].children.entries().next().value[1]
  }
  // ESPERO QUE LOS PROPS ESTEN DEFINIDOS
  expect(name).toBeDefined()
  expect(label).toBeDefined()
  //ESPERO QUE LOS PROPS LLEGUEN COMO TIPO STRING
  expect.stringContaining(name)
  expect.stringContaining(label)
})
