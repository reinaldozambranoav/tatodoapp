import { useState, useRef, useEffect } from 'react'

import ReactCrop, {
  centerCrop,
  makeAspectCrop,
} from 'react-image-crop'
import canvasPreview from './canvasPreview'
import useDebounceEffect from './useDebounceEffect'

import 'react-image-crop/dist/ReactCrop.css'
import { Grid } from '@mui/material'
import useWindowSize from 'helper/UseWindowsSize'

// This is to demonstate how to make and center a % aspect crop
// which is a bit trickier so we use some helper functions.
const centerAspectCrop = ({ mediaWidth, mediaHeight }) => {
  return centerCrop(
    makeAspectCrop(
      {
        unit: '%',
        width: 90,
      },

      mediaWidth,
      mediaHeight,
    ),
    mediaWidth,
    mediaHeight,
  )
}
const isShowCanva = false
const TaReactCrop = ({ imgSrc, onCallback }) => {
  // const [imgSrc, setImgSrc] = useState(src)
  const previewCanvasRef = useRef(null)
  const imgRef = useRef(null)
  const [crop, setCrop] = useState({
    unit: "px",
    x: 25,
    y: 25,
    width: 50,
    height: 50,
  })
  const [completedCrop, setCompletedCrop] = useState({
    unit: "px",
    x: 0,
    y: 0,
    width: 0,
    height: 0,
  })
  const [scale, setScale] = useState(1)
  const [rotate, setRotate] = useState(0)
  const [aspect, setAspect] = useState(16 / 9)
  const { height: sizeHeight } = useWindowSize()


  // const onSelectFile = (e) => {
  //   if (e.target.files && e.target.files.length > 0) {
  //     setCrop(undefined) // Hace que la vista previa de recorte se actualice entre imágenes.
  //     const reader = new FileReader()
  //     reader.addEventListener('load', () =>
  //       setImgSrc(reader.result.toString() || ''),
  //     )
  //     reader.readAsDataURL(e.target.files[0])
  //   }
  // }

  const onImageLoad = (e) => {
    // console.log('imgRef', imgRef)
    // const { width, height } = e.currentTarget
    // if (aspect) {
    const { width, height } = e.currentTarget
    console.log(aspect, width, height)
    setCrop(centerAspectCrop(width, height))
    // }
  }

  useDebounceEffect(
    async () => {
      if (
        completedCrop?.width &&
        completedCrop?.height &&
        imgRef.current &&
        previewCanvasRef.current
      ) {
        // Usamos canvasPreview porque es mucho más rápido que imgPreview.
        canvasPreview(
          imgRef.current,
          previewCanvasRef.current,
          completedCrop,
          scale,
          rotate,
        )
      }
    },
    100,
    [completedCrop, scale, rotate],
  )

  // const fnToggleAspectClick = () => {
  //   if (aspect) {
  //     setAspect(undefined)
  //   } else if (imgRef.current) {
  //     const { width, height } = imgRef.current
  //     setAspect(16 / 9)
  //     setCrop(centerAspectCrop(width, height, 16 / 9))
  //   }
  // }


  const fnOnCallbackImage = (c) => {
    // onCallback(c)
    setCompletedCrop(c)
  }

  useEffect(() => {
    if (completedCrop) onCallback(previewCanvasRef.current)

  }, [completedCrop])

  return (
    <>
      <Grid container id={'reactCropContainer'}>
        <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align="center" sx={{paddingTop:'50px'}}>
          <ReactCrop
            crop={crop}
            onChange={(_, percentCrop) => setCrop(percentCrop)}
            onComplete={(c) => fnOnCallbackImage(c)}
          // aspect={aspect}
          >
            <img ref={imgRef} src={imgSrc} alt=" "
              height={sizeHeight - 425} onLoad={onImageLoad}
            // style={{ transform: `scale(${scale}) rotate(${rotate}deg)` }}
            />
          </ReactCrop>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12} xl={12} hidden={true}>
          <canvas
            ref={previewCanvasRef}
            style={{
              border: '1px solid black',
              objectFit: 'contain',
              width: completedCrop.width,
              height: completedCrop.height,
            }}
          />
        </Grid>
      </Grid>
    </>
  )
}
export default TaReactCrop




{/* <Grid className="Crop-Controls">
        <input type="file" accept="image/*" onChange={onSelectFile} />
        <div>
          <label htmlFor="scale-input">Scale: </label>
          <input
            id="scale-input"
            type="number"
            step="0.1"
            value={scale}
            disabled={!imgSrc}
            onChange={(e) => setScale((e.target.value))}
          />
        </div>
        <div>
          <label htmlFor="rotate-input">Rotate: </label>
          <input
            id="rotate-input"
            type="number"
            value={rotate}
            disabled={!imgSrc}
            onChange={(e) =>
              setRotate(Math.min(180, Math.max(-180, (e.target.value))))
            }
          />
        </div>
        <div>
          <button onClick={fnToggleAspectClick}>
            Toggle aspect {aspect ? 'off' : 'on'}
          </button>
        </div>
      </Grid> */}