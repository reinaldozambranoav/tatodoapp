
const TO_RADIANS = Math.PI / 180

const canvasPreview = async (image, canvas, crop, scale = 1, rotate = 0) => {
    const ctx = canvas.getContext('2d')

    if (!ctx) {
        throw new Error('No 2d context')
    }

    const scaleX = image.naturalWidth / image.width
    const scaleY = image.naturalHeight / image.height
    // devicePixelRatio aumenta ligeramente la nitidez en dispositivos retina
    // a expensas de tiempos de renderizado ligeramente más lentos y la necesidad de
    // reduzca el tamaño de la imagen si desea descargar/cargar y ser
    // fiel al tamaño natural de las imágenes.
    const pixelRatio = window.devicePixelRatio
    // const pixelRatio = 1

    canvas.width = Math.floor(crop.width * scaleX * pixelRatio)
    canvas.height = Math.floor(crop.height * scaleY * pixelRatio)

    ctx.scale(pixelRatio, pixelRatio)
    ctx.imageSmoothingQuality = 'high'

    const cropX = crop.x * scaleX
    const cropY = crop.y * scaleY

    const rotateRads = rotate * TO_RADIANS
    const centerX = image.naturalWidth / 2
    const centerY = image.naturalHeight / 2

    ctx.save()

    // 5) Mover el origen del recorte al origen del lienzo (0,0)
    ctx.translate(-cropX, -cropY)
    // 4)Mover el origen al centro de la posición original
    ctx.translate(centerX, centerY)
    // 3) Rotar alrededor del origen
    ctx.rotate(rotateRads)
    // 2) Escalar la imagen
    ctx.scale(scale, scale)
    // 1)Mover el centro de la imagen al origen (0,0)
    ctx.translate(-centerX, -centerY)
    ctx.drawImage(
        image,
        0,
        0,
        image.naturalWidth,
        image.naturalHeight,
        0,
        0,
        image.naturalWidth,
        image.naturalHeight,
    )

    ctx.restore()
}
export default canvasPreview