import renderer from 'react-test-renderer'
import TaButton from './TaButton';


test('component test button', () => {

    const TaButton1 = renderer.create(<TaButton label={'3'} />);

    let tree = TaButton1.toJSON()
    let label = tree.children[0]
    expect(label).toBeDefined()
    expect.stringContaining(label)
})
