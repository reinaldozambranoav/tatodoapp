import { Accordion, AccordionDetails, AccordionSummary } from '@mui/material'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { memo, useState } from 'react'
import { makeStyles } from '@mui/styles'
import TaTypography from 'components/typography/TaTypography'

const expansionPanelStyles = makeStyles({
  root: {
    backgroundColor: 'var(--main-bg-color1)',
    minHeight: '50px',
    fontFamily: 'Archivo',
    fontSize: "14pt",
    color: "var(--main-text-color0)",
  },
  content: {
    padding: '5px 5px 5px 10px',
    backgroundColor: 'var(--main-bg-color0)'
  },
})

const TaExpansionPanel = memo(({ id, label, disabled, children }) => {

  const [expanded, setExpanded] = useState(false)

  const classes = expansionPanelStyles()

  const fnExpand = (panel) => (event, isExpanded) => setExpanded(isExpanded ? panel : false)

  return (
    <Accordion id={`taExpPanel-${id}`} expanded={expanded === id} classes={{ root: classes.root }} onChange={fnExpand(id)} disabled={disabled ?? false} >
      <AccordionSummary id={`taExpPanel-label-${id}`} expandIcon={<ExpandMoreIcon />}>
        <TaTypography text={label} color={'var(--main-bg-color0)'} fontSize={'15pt'} />
      </AccordionSummary>
      <AccordionDetails id={`taExpPanel-content-${id}`} classes={{ root: classes.content }}>
        {children}
      </AccordionDetails>
    </Accordion>
  )
})

export default TaExpansionPanel