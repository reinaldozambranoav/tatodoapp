import { memo } from 'react'
import PropTypes from 'prop-types'
import { Drawer } from "@mui/material"
import { makeStyles } from '@mui/styles'

const TaDrawerStyles = makeStyles({
    paperAnchorLeft: {
        width: props => props.width,
        overflow: 'hidden',
    },
})

const TaDrawer = memo(({ id, isOpen, onClose, position, children, width }) => {
    const classes = TaDrawerStyles({ width: width })
    return (
        <Drawer id={id} anchor={position} onClose={onClose} open={isOpen} classes={{ paperAnchorLeft: classes.paperAnchorLeft }}>
            {children}
        </Drawer>
    )
})

TaDrawer.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    position: PropTypes.string,
    width: PropTypes.number,
    id: PropTypes.string
}

TaDrawer.defaultProps = {
    position: 'left',
    width: 200
}

export default TaDrawer