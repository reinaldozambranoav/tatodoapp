import HomeLanguages from 'views/Home/HomeLanguages'
import { CASH_REGISTER_CLOSURE, CONFIGURATION, FISCAL_PRINTER, ORDER_TOUCH, SALES } from 'constants/appRoutes'
import { LoginContext } from 'contexts/Login/LoginContext'
import { useContext } from 'react'
import { LanguageContext } from 'contexts/Language/LanguageContext'
import { PermissionContext } from 'contexts/Permissions/PermissionContext'
import { useState, useEffect } from 'react'
import cashClosureLanguages from 'proyectComponents/CashClosure/components/cashClosureLanguages'
import { documentList } from 'constants/DocumentList'
import useCurrentPermissionV2 from 'contexts/Permissions/hooks/useCurrentPermissionV2'
import { StationContext } from 'contexts/Station/StationContext'

const { menuItemLang: { salesLang, configurationLang, configGeneralLang, configFiscalPrinterLang, cashClosureLang,
  cashClosureDailyReportLang, priceVisorLang, offlineLang,
  offlineDocsLang, offlineMastersLang, printerLang, orderTouchLang,
  restaurantLang, printTransactionsLang, dispatchConfirmationLang, sitefAdminLang,
  vposAdminLang, sheetCleanupLang, cxcLang, currencyFactorLang,
  aboutUsLang, commandsConfigLang, mastersLoadConfigLang } } = HomeLanguages
const { menuCashClosureLang } = cashClosureLanguages

const fnHomeMenu = (languageState, homePolitics, configSp, stationType) => {
  const { sitef: { isSitef }, isVpos, isSheet, isExpress } = configSp

  const fnGetIsVisibleMenuOption = (id, subId, configValue) => {
    if (subId === undefined) {
      return (configValue !== undefined && configValue === true)
        ? (configValue === true && homePolitics?.find(x => String(x.id) === String(id))?.active) || false
        : homePolitics?.find(x => String(x.id) === String(id))?.active ?? false
    }
    else return (configValue !== undefined && configValue === true)
      ? (configValue === true && homePolitics?.find(x => String(x.id) === String(id))?.subPolitics?.find(x => String(x.id) === String(subId))?.active) || false
      : homePolitics?.find(x => String(x.id) === String(id))?.subPolitics?.find(x => String(x.id) === String(subId))?.active ?? false
  }

  return [
    {
      id: 0,
      name: salesLang[languageState],
      expand: false,
      nextPath: SALES,
      visible: fnGetIsVisibleMenuOption(1),
    },
    {
      id: 1,
      name: configurationLang[languageState],
      expand: true,
      visible: fnGetIsVisibleMenuOption(2),
      isExpanded: false,
      subMenu: [
        {
          id: 10,
          name: configGeneralLang[languageState],
          nextPath: CONFIGURATION
        },
        {
          id: 11,
          name: configFiscalPrinterLang[languageState],
          nextPath: FISCAL_PRINTER
        },
        {
          id: 12,
          name: commandsConfigLang[languageState],
        },
        {
          id: 13,
          name: mastersLoadConfigLang[languageState],
        },
      ],
    },
    {
      id: 2,
      name: cashClosureLang[languageState],
      expand: false,
      nextPath: CASH_REGISTER_CLOSURE,
      visible: fnGetIsVisibleMenuOption(3),
    },
    {
      id: 23,
      name: cashClosureDailyReportLang[languageState],
      expand: false,
      nextPath: '',
      visible: fnGetIsVisibleMenuOption(22),
    },
    {
      id: 999,
      name: priceVisorLang[languageState],
      expand: false,
      visible: (stationType === 1)
    },
    {
      id: 3,
      name: offlineLang[languageState],
      expand: true,
      isExpanded: false,
      visible: (stationType === 1) || fnGetIsVisibleMenuOption(4),
      subMenu: [
        {
          id: 31,
          name: offlineDocsLang[languageState],
          nextPath: ''
        },
        {
          id: 32,
          name: offlineMastersLang[languageState],
          nextPath: ''
        }
      ],
    },
    {
      id: 4,
      name: printerLang[languageState],
      expand: false,
      visible: fnGetIsVisibleMenuOption(5),
    },
    {
      id: 5,
      name: orderTouchLang[languageState],
      expand: false,
      nextPath: ORDER_TOUCH,
      visible: fnGetIsVisibleMenuOption(6),
    },
    {
      id: 50,
      name: restaurantLang[languageState],
      expand: false,
      visible: fnGetIsVisibleMenuOption(17),
    },
    {
      id: 16,
      name: printTransactionsLang[languageState],
      expand: false,
      nextPath: '',
      visible: fnGetIsVisibleMenuOption(16),
    },
    {
      id: 25,
      name: dispatchConfirmationLang[languageState],
      expand: false,
      nextPath: '',
      visible: fnGetIsVisibleMenuOption(21),
    },
    {
      id: 70,
      name: sitefAdminLang[languageState],
      expand: false,
      nextPath: '',
      visible: fnGetIsVisibleMenuOption(8, undefined, isSitef),
    },
    {
      id: 71,
      name: vposAdminLang[languageState],
      expand: false,
      nextPath: '',
      visible: fnGetIsVisibleMenuOption(9, undefined, isVpos),
    },
    {
      id: 72,
      name: 'Merchant',
      expand: false,
      nextPath: '',
      visible: fnGetIsVisibleMenuOption(8, undefined, isSitef) || fnGetIsVisibleMenuOption(9, undefined, isVpos),
    },
    {
      id: 80,
      name: sheetCleanupLang[languageState],
      expand: false,
      visible: fnGetIsVisibleMenuOption(10, undefined, isSheet),
    },
    {
      id: 97,
      name: cxcLang[languageState],
      expand: false,
      nextPath: '',
      visible: fnGetIsVisibleMenuOption(12),
    },
    {
      id: 98,
      name: currencyFactorLang[languageState],
      expand: false,
      nextPath: '',
      visible: fnGetIsVisibleMenuOption(11),
    },
    {
      id: 99,
      name: aboutUsLang[languageState],
      expand: false,
      visible: true,
    },
  ]
}

const fnSalesMenu = (languageState, doc) => {
  try {
    let newDocuments = documentList.filter(x => doc.some(y => y.id === x.id))
    return newDocuments.map(x => ({
      ...x,
      name: documentList.find(s => x.id === s.id).name[languageState],
      expand: false,
      visible: true
    }))
  } catch (error) {
    console.error('fnSalesMenu => ', error)
    return []
  }
}

const fnCashClosureMenu = (languageState, lvlId, cashBox, configSp) => {
  const { isExpress } = configSp

  return [
    {
      id: 0,
      name: menuCashClosureLang[0][languageState],
      expand: false,
      visible: true,
      active: cashBox[lvlId].key[0]['phase1'].isVisible
    },
    {
      id: 1,
      name: menuCashClosureLang[1][languageState],
      expand: false,
      visible: true,
      active: (isExpress === true) ? false : cashBox[lvlId].key[0]['phase2'].isVisible
    },
    {
      id: 2,
      name: menuCashClosureLang[2][languageState],
      expand: false,
      visible: true,
      active: cashBox[lvlId].key[0]['phase3'].isVisible
    },
  ]
}

const useMenuButton = (location) => {

  const { languageState } = useContext(LanguageContext)
  const { loginState: { lvlId, isLogin } } = useContext(LoginContext)
  const { permissionState } = useContext(PermissionContext)
  const { cashBox, configSp, saint } = permissionState
  const { stationState: { stationType } } = useContext(StationContext)

  const [menuArray, setMenuArray] = useState([])

  const { getCurrentHomePolitics, getCurrentDocPolitics } = useCurrentPermissionV2()
  const homePolitics = getCurrentHomePolitics()
  const doc = getCurrentDocPolitics()

  useEffect(() => {
    let array = []
    if (isLogin) {
      switch (location) {
        case 'home':
          array = fnHomeMenu(languageState, homePolitics, configSp, stationType)
          break
        case 'sales':
          array = fnSalesMenu(languageState, doc)
          break
        case 'cashClosure':
          array = fnCashClosureMenu(languageState, lvlId, cashBox, configSp)
          break
        default:
          break
      }
    }
    setMenuArray(array)
  }, [isLogin, permissionState])

  return menuArray
}

export default useMenuButton