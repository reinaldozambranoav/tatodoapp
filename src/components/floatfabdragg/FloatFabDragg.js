import React, { memo } from 'react';
import PropTypes from 'prop-types';
import Draggable from 'react-draggable';
import TaFab from '../fab/TaFab';

const FloatFabDragg = memo(({ isOpen, onClick, icon, color }) => {
    return (
        <Draggable
            bounds="body">
            <div style={{ position: 'fixed', top: '20px' }}>
                <TaFab
                    id="btnFloatFabDragg"
                    icon={icon}
                    hidden={isOpen}
                    onClick={(onClick) && onClick}
                    color={color} />
            </div>
        </Draggable>
    );
});

FloatFabDragg.propTypes = {
    onClick: PropTypes.func,
    icon: PropTypes.string,
    color: PropTypes.string,
    isOpen: PropTypes.bool.isRequired,
};

FloatFabDragg.defaultProps = {
    icon: "arrow",
    color: "blue",
}

export default FloatFabDragg;