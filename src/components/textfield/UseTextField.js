import moment from 'moment';
import { useEffect, useState } from 'react';
import {
    textfieldStyles,
    textfieldLabelStyles,
    helperTextStyles
} from "./TextField";

const useTextField = (name, label, errors, type, placeholder = '', maxLength, onKeyDown, type2, textAlign, helperText, fontSize, color) => {

    // console.log('entre en useTextField1 => ', name)
    const textfieldClasses = textfieldStyles({ fontSize, color });
    const textfieldLabelClasses = textfieldLabelStyles({ color });
    const helperTextClasses = helperTextStyles()
    const [ textfield, setTextfield ] = useState({
        name: name,
        label: label,
        helperText: errors ? errors[ name ]?.message : helperText,
        error: errors ? (errors[ name ]) : false,
        placeholder: placeholder,
        variant: 'standard',
        InputProps: {
            id: name,
            classes: { input: textfieldClasses.input, underline: textfieldClasses.underline },
        },
        InputLabelProps: {
            classes: { root: textfieldLabelClasses.root, focused: textfieldLabelClasses.focused, },
            shrink: true,
        },
        inputProps: {
            maxLength: maxLength ?? 524288, min: type === 'number' ? "0" :
                type === 'date' ? "1900-01-01" : undefined, max: type === 'date' ? moment(new Date()).format("YYYY-MM-DD") :
                    type2 === 'numberZip' ? '9999' : type2 === 'docId' ? '9999999999' : undefined, step: type === 'number' ? "1" : undefined,
            style: { textAlign: (textAlign) ? textAlign : 'left' }
        },
        FormHelperTextProps: {
            classes: { root: helperTextClasses.root, disabled: helperTextClasses.disabled }
        },
        type: type,
        onKeyDown: onKeyDown ? onKeyDown : undefined,
        autoComplete: 'off'
    })

    useEffect(() => {
        // console.log('entre en useTextField2 => ', name)
        setTextfield(preVal => ({
            ...preVal,
            helperText: errors ? errors[ name ]?.message ?? helperText : helperText,
            error: errors ? (errors[ name ]) ?? false : false,
            label: label ?? '',
            placeholder: placeholder,
            type: type
        }))
    }, [ errors, name, label, helperText, placeholder, type ])

    // useEffect(() => {
    //     setTextfield(preVal => ({
    //         ...preVal,
    //         helperText: errors ? errors[name]?.message ?? helperText : helperText,
    //         error: errors ? (errors[name]) ?? false : false,
    //         label: label ?? ''
    //             }))
    // })

    return textfield;
}

export default useTextField;