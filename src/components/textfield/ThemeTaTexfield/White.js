import { makeStyles } from "@mui/styles";

export const whiteStyles = makeStyles({
    input:
    {
        fontFamily: "Archivo",
        fontSize: "12pt",
        color: 'var(--main-text-color1)',
        '&:hover': {
            backgroundColor: 'transparent !important',
        },
    },
    underline: {
        '&$focused': {
            borderBottom: '1px solid var(--main-hover-color0)'
        },
        '&:hover': {
            borderBottom: '1px solid var(--main-hover-color0)',
        },
        '&:hover:not(.Mui-disabled):before': {
            borderBottom: '2px solid var(--main-hover-color0)',
        },
        '&:before': {
            borderBottom: '2px solid var(--main-text-color3)',
        },
        '&:after': {
            borderBottom: '1px solid var(--main-hover-color0)',
        }
    },
})

export const whiteLabelStyles = makeStyles({
    root: {
        fontFamily: "Archivo",
        fontSize: "12pt",
        color: props => props.color ?? 'var(--main-text-color1)',
        '&:hover': {
            backgroundColor: 'transparent !important',
            color: props => props.color ?? 'var(--main-bg-color0)',
        },
        '&$focused': {
            color: 'var(--main-bg-color0)',
            margin: '0px !important',
        },
        '&$.Mui-error': {
            color: 'var(--main-bg-color9)'
        },
        textAlign: props => props.textAlign ?? 'left'
    },
    focused: {
        color: 'var(--main-bg-color9)',
        fontFamily: "Archivo",
        fontSize: "12pt",
        backgroundColor: 'transparent !important',
        '&$focused': {
            backgroundColor: 'transparent !important',
            color: props => props.color ?? 'var(--main-bg-color0)',
        },
        textAlign: props => props.textAlign ?? 'left'
    }
});
