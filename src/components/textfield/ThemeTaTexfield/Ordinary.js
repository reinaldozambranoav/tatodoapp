import { makeStyles } from "@mui/styles";

export const ordinaryStyles = makeStyles({
    input:
    {
        fontFamily: "Archivo",
        fontSize: "12pt",
        color: 'var(--main-text-color0)',
        '&:hover': {
            backgroundColor: 'transparent !important',
        },
    },
    underline: {
        '&$focused': {
            borderBottom: '1px solid var(--main-hover-color0)'
        },
        '&:hover:not(.Mui-disabled):before': {
            borderBottom: '2px solid var(--main-hover-color0)',
        },
        '&:before': {
            borderBottom: '2px solid var(--main-bg-color1)',
        },
        '&:after': {
            borderBottom: '2px solid var(--main-hover-color0)',
        }
    },
})


export const ordinaryLabelStyles = makeStyles({
    root: {
        fontFamily: "Archivo",
        fontSize: "12pt",
        color: props => props.color ?? 'var(--main-text-color0)',
        '&:hover': {
            backgroundColor: 'transparent !important',
            color: props => props.color ?? 'var(--main-bg-color1)',
        },
        '&$focused': {
            color: 'var(--main-bg-color1)',
            margin: '0px !important',
        },
        '&$.Mui-error': {
            color: 'var(--main-bg-color9)'
        },
        textAlign: props => props.textAlign ?? 'left'
    },
    focused: {
        color: 'var(--main-bg-color9)',
        fontFamily: "Archivo",
        fontSize: "12pt",
        backgroundColor: 'transparent !important',
        '&$focused': {
            backgroundColor: 'transparent !important',
            color: props => props.color ?? 'var(--main-bg-color1)',
        },
        textAlign: props => props.textAlign ?? 'left'
    }
});