import React, { useState, useEffect, useRef } from 'react'
import {
    Dialog,
    DialogContent,
    Grid,
} from '@mui/material'

import {
    withStyles
} from '@mui/styles'

import Keyboard from "react-simple-keyboard"

import "react-simple-keyboard/build/css/index.css";
import './Keyboard.css'

import TaTextField from './TaTextFieldOld'

import TaFab from "components/fab/TaFab";

import {
    makeStyles,
} from '@mui/styles'

const DialogStyles = makeStyles({
    content: {
        margin: "0px",
        padding: "5px !important",
    },
})

let isOpenModal = false

const ModalKeyboard = React.memo(({ open, onClose, isNumeric,
    name, isPassword, onChangeTextField, onKeyDownTextField, currentValue }) => {
    const classes = DialogStyles();
    const [isOpen, setIsOpen] = useState(false)
    const [layout, setLayout] = useState(isNumeric === false ? 'default' : 'numeric')
    const keyboard = useRef()
    const [input, setInput] = useState('')

    const onCloseKeyboard = (button) => {
        isOpenModal = false
        if (onKeyDownTextField !== undefined && name !== undefined) {
            const responseTxtKeyDown = {
                key: button === '%' ? '%' : 'Enter',
                target: {
                    id: name,
                    name: name,
                    value: keyboard.current.input.default + (button === '%' ? '%' : '')
                }
            }
            //console.log('responseTxtKeyDown => ', responseTxtKeyDown)
            onKeyDownTextField(responseTxtKeyDown)
        }

        setIsOpen(false)
        onClose(name, keyboard.current.input.default, (button) ? (button === 'Enter') : false )
    }

    const onChange = (input) => {
        if (isOpenModal === true) {
            setInput(input)
            if (onChangeTextField !== undefined) {
                if (isNumeric) {
                    const response1 = {
                        floatValue: (+input)
                    }
                    const response2 = {
                        target: {
                            name: name,
                        }
                    }

                    onChangeTextField(response1, response2)

                } else {
                    const responseTxt = {
                        target: {
                            name: name,
                            value: input
                        }
                    }
                    onChangeTextField(responseTxt)
                }
            }
        }
    }
    const onKeyPress = (button) => {
        switch (button) {
            case "{shift}":
            case "{lock}":
                handleShift();
                break;
            case '{enter}':
            case '%':
                onCloseKeyboard(button)
                break;
            default:
                return
        }
    }

    const handleShift = () => {
        const newLayoutName = layout === "default" ? "shift" : "default";
        setLayout(newLayoutName);
    };

    useEffect(() => {
        if (open === true && isOpen === false) {
            setIsOpen(true)
            setInput(currentValue !== undefined ? currentValue : '')
            isOpenModal = true
            //console.log('useEffect => ', keyboard.current.setInput('Hola'))
        }
    }, [open, name])

    return (
        <Dialog
            open={isOpen}
            maxWidth={'md'}>
            <DialogContent className={classes.content}>
                <Grid container spacing={1}>
                    <Grid item xs={10} sm={10} md={11} lg={11} xl={11}>
                        <TaTextField
                            id='txtKeyboard'
                            disabled={true}
                            value={input}
                            fullWidth
                            type={(isPassword) ? "password" : "text"}
                        />
                    </Grid>
                    <Grid item xs={2} sm={2} md={1} lg={1} xl={1}>
                        <TaFab
                            id='txtCloseVirtualKeyboard'
                            icon="exit"
                            color="red"
                            onClick={onCloseKeyboard}/>
                    </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Keyboard
                            keyboardRef={r => {
                                keyboard.current = r
                                keyboard.current.setInput(currentValue !== undefined ? currentValue : '')
                            }}
                            layoutName={layout}
                            theme={layout === "default" ? "hg-theme-default hg-layout-default myTheme" : "hg-theme-default hg-layout-default myTheme simple-numericKeyboard"}
                            layout={{
                                'default': [
                                    '` 1 2 3 4 5 6 7 8 9 0 - = {bksp}',
                                    '{tab} q w e r t y u i o p [ ] \\',
                                    '{lock} a s d f g h j k l ; \' {enter}',
                                    '{shift} z x c v b n m , . / {shift}',
                                    '.com @ {space}'
                                ],
                                'shift': [
                                    '~ ! @ # $ % ^ & * ( ) _ + {bksp}',
                                    '{tab} Q W E R T Y U I O P { } |',
                                    '{lock} A S D F G H J K L : " {enter}',
                                    '{shift} Z X C V B N M < > ? {shift}',
                                    '.com @ {space}'
                                ],
                                'numeric': [
                                    '7 8 9 %',
                                    '4 5 6 {bksp}',
                                    '1 2 3 {enter}',
                                    '0 00 000 .',
                                ]
                            }}
                            buttonTheme={[
                                {
                                    class: "hg-red",
                                    buttons: '` 1 2 3 4 5 6 7 8 9 0 00 000 - = {bksp} ~ ! @ # $ % ^ & * ( ) _ + {tab} q w e r t y u i o p [ ] Q W E R T Y U I O P { } | \\ {lock} a s d f g h j k l ; \' A S D F G H J K L : {enter} {shift} z x c v b n m , . / Z X C V B N M < > ? .com @ {space} % "'
                                },
                            ]}
                            display={{
                                '{bksp}': 'Del',
                                '{enter}': 'Enter',
                                '{shift}': 'Shift',
                                '{lock}': 'Caps',
                                '{tab}': 'Tab',
                                '{space}': 'Space'
                            }}
                            onChange={onChange}
                            onKeyPress={onKeyPress}
                        />
                    </Grid>
                </Grid>
            </DialogContent>
        </Dialog>
    )
})

export default ModalKeyboard