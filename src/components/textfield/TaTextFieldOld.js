import React, { memo, useCallback } from 'react';
import PropTypes from 'prop-types';
import TextField from "@mui/material/TextField";
import useTextField from "./UseTextField";

const onFocus = (e) => {
  e.target.select()
}

const TaTextField = memo(({ name, label, helperText, register, fullWidth,
  errors, type, disabled, autoFocus, validate,
  onBlur, placeholder = "", pattern, required, onChange, onKeyDown, textAlign, fontSize, color}) => {

  // console.log('TaTextField errors => ', errors)
  const txt = useTextField(name, label, errors, type, placeholder, undefined, "", undefined, textAlign, helperText, fontSize, color)
  // console.log('txt => ', txt)

  const fnOnBlur = useCallback((event) => {
    if (onBlur) {
      onBlur(event)
    }
  }, [onBlur])

  const fnOnChange = useCallback((event) => {
    if (onChange) {
      onChange(event)
    }
  }, [onChange])

  const fnOnKeyDown = useCallback((event) => {
    if (onKeyDown) {
      onKeyDown(event)
    }
  }, [onKeyDown])

  return (
    <>
      {register ? <TextField
        {...txt}
        {...register(name,
          {
            required: required && required,
            pattern: pattern && pattern,
            validate: validate && validate
          })}
        fullWidth={fullWidth}
        autoFocus={autoFocus}
        autoComplete={type === 'password' ? 'new-password' : 'off'}
        onBlur={fnOnBlur}
        disabled={disabled}
        onFocus={onFocus}
        onKeyDown={fnOnKeyDown}
      /> : <TextField
        {...txt}
        fullWidth={fullWidth}
        autoFocus={autoFocus}
        autoComplete={type === 'password' ? 'new-password' : 'off'}
        onChange={fnOnChange}
        onBlur={fnOnBlur}
        disabled={disabled}
        onFocus={onFocus}
      />
      }
    </>
  )
})

TaTextField.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  register: PropTypes.func,
  errors: PropTypes.object,
  type: PropTypes.string,
  fullWidth: PropTypes.bool,
  disabled: PropTypes.bool,
  placeholder: PropTypes.string,
};

TaTextField.defaultProps = {
  type: 'text',
  label: '',
  fullWidth: true,
  disabled: false,
  autoFocus: false,
  placeholder: "",
  helperText: " ",
}

export default TaTextField;