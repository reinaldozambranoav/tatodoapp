import React, { memo } from 'react'
import PropTypes from 'prop-types'
import { MenuItem, TextField, } from "@mui/material/"
import useTextField from "./UseTextField"
import menuItemStyle from "../styles/MenuItem"
import { Controller } from 'react-hook-form'

const TaListTextField = memo(({ id, name, label, errors, options,
  fullWidth, disabled, required, control,
  value, fnOnChange, defaultValue, testId,
  helperText, color
}) => {
  
  const txt = useTextField(name, label, errors, 'text', '', 25, undefined, 'text', 'left', helperText, '', color)
  const classes = menuItemStyle({ color })

  const onChange = (event) => fnOnChange(event.target.name, event.target.value)
  return (
    <>
      {control ? <Controller
        name={name}
        control={control}
        render={({
          field: { onChange, onBlur, value, name, ref },
          fieldState: { invalid, isTouched, isDirty, error },
        }) => <TextField
          {...txt}
          onChange={onChange}
          helperText={(!errors) && helperText}
          value={value}
          inputRef={ref}
          id={id}
          select
          fullWidth={fullWidth}
          rules={{ required: required }}
          disabled={disabled}>
            {
              options.map(option => <MenuItem classes={{ root: classes.root }} key={option.id} value={option.id}>{option.name}</MenuItem>)
            }
          </TextField>}
      />
        : <TextField
          {...txt}
          select
          fullWidth={fullWidth}
          onChange={onChange}
          value={value}
          defaultValue={defaultValue}
          test-id={testId ?? ''}
          disabled={disabled}>
          {
            options.map(option => <MenuItem classes={{ root: classes.root }} key={option.id} value={option.id}>{option.name}</MenuItem>)
          }
        </TextField>}
    </>
  )
})

TaListTextField.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  errors: PropTypes.object,
  options: PropTypes.array,
  fullWidth: PropTypes.bool,
  disabled: PropTypes.bool,
  control: PropTypes.object,
}

TaListTextField.defaultProps = {
  options: [],
  fullWidth: true,
  disabled: false,
  label: ' ',
  helperText: ''
}

export default TaListTextField