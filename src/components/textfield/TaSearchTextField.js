import { Autocomplete, Grid, ListItemButton } from "@mui/material"
import TaTextField from "components/textfield/TaTextField"
import { fnIncludeStrings } from "helper/CompareValues"
import { useMemo, memo, useEffect } from "react"
import { Controller, useForm } from "react-hook-form"
import { fnDefineTheme } from "./ThemeDefinition"
import { makeStyles } from "@mui/styles"
import { v4 as uuid } from 'uuid'

const taSearchTextFieldStyles = makeStyles({
  listBox: {
    fontFamily: 'Archivo',
    fontSize: "11pt",
    color: "var(--main-text-color0)",
  }
})

const TaSearchTextField = memo(({
  label = '', placeholder = '', isDisabled = false, data = [],
  extName, extControl, fullWidth = true,
  autoFocus = false, id, helperText = '',
  theme = 'ordinary', desiredId = 'name',
  desiredLabel = 'name', detailedOptions = false, getValue,
  desiredName = 'name'
}) => {
  
  const { control, formState: { errors }, watch } = useForm({ defaultValues: { search: '' } })
  const preparedData = useMemo(() => data.map(x => ({ key: uuid(), label: x[desiredLabel], id: x[desiredId], name: x[desiredName] })), [data])
  const textfieldClasses = fnDefineTheme(theme)
  const classes = taSearchTextFieldStyles()

  useEffect(() => {
    if (extControl === undefined && watch('search') !== '') getValue(watch('search'))
  }, [watch('search')])

  return <>
    {(extControl !== undefined)
      ? <Controller
        name={extName}
        control={extControl}
        render={({ field }) => (<Autocomplete
          id={id}
          value={field.value}
          classes={{ listbox: classes.listBox }}
          onChange={(event, value, reason) => {
            if (reason === 'clear') return field.onChange('')
            if (reason === 'selectOption') return field.onChange(value.id)
          }}
          autoHighlight={fnIncludeStrings(preparedData, field.value)}
          onKeyDown={(e) => {
            if (e.key === 'Enter') {
              field.onChange(e.target.value)
            }
          }}
          renderOption={(props, option) => {
            let newProps = { ...props }
            delete newProps.className
            return (<ListItemButton {...newProps} key={option.key} >
              {(detailedOptions)
                ? <Grid container >
                  <Grid item xs={12} sm={12} md={12} lg={12} xl={12}> {option.id} </Grid>
                  <Grid item xs={12} sm={12} md={12} lg={12} xl={12}> {option.name} </Grid>
                </Grid>
                : option.label}
            </ListItemButton>)
          }}
          options={preparedData}
          disabled={isDisabled}
          renderInput={(params) => {
            delete params.InputProps.className
            delete params.inputProps.className
            params = {
              ...params,
              InputProps: {
                ...params.InputProps,
                classes: {
                  input: textfieldClasses.input,
                  underline: textfieldClasses.underline
                }
              }
            }
            return (<TaTextField
              id={'searchField'}
              label={label}
              customInputProps={params.InputProps}
              customLowKeyInputProps={params.inputProps}
              placeholder={placeholder}
              helperText={errors[extName]?.message ?? helperText}
              fullWidth={fullWidth}
              autoFocus={autoFocus}
              disabled={isDisabled}
            />)
          }}
        />)}
      />
      : <Controller
        name={'search'}
        control={control}
        render={({ field }) => {
          return (<Autocomplete
            id={id}
            value={field.value}
            classes={{ listbox: classes.listBox }}
            onChange={(event, value, reason) => {
              if (reason === 'clear') return field.onChange('')
              if (reason === 'selectOption') return field.onChange(value.id)
            }}
            autoHighlight={fnIncludeStrings(preparedData, field.value)}
            onKeyDown={(e) => {
              if (e.key === 'Enter') {
                field.onChange(e.target.value)
              }
            }}
            renderOption={(props, option) => {
              let newProps = { ...props }
              delete newProps.className
              return (<ListItemButton {...newProps} key={option.key} >
                {(detailedOptions)
                  ? <Grid container >
                    <Grid item xs={12} sm={12} md={12} lg={12} xl={12}> {option.id} </Grid>
                    <Grid item xs={12} sm={12} md={12} lg={12} xl={12}> {option.name} </Grid>
                  </Grid>
                  : option.label}
              </ListItemButton>)
            }}
            options={preparedData}
            disabled={isDisabled}
            renderInput={(params) => {
              delete params.InputProps.className
              delete params.inputProps.className
              params = {
                ...params,
                InputProps: {
                  ...params.InputProps,
                  classes: {
                    input: textfieldClasses.input,
                    underline: textfieldClasses.underline
                  }
                }
              }
              return (<TaTextField
                id={'searchField'}
                label={label}
                customInputProps={params.InputProps}
                customLowKeyInputProps={params.inputProps}
                placeholder={placeholder}
                helperText={errors[extName]?.message ?? helperText}
                fullWidth={fullWidth}
                autoFocus={autoFocus}
                disabled={isDisabled}
              />)
            }}
          />)
        }}
      />
    }
  </>
})

export default TaSearchTextField