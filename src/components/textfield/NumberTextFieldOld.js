import { memo } from "react";
import PropTypes from 'prop-types';
import NumberFormat from "react-number-format";
import {
    TextField,
} from "@mui/material";
import useTextField from "./UseTextField";
import { Controller } from "react-hook-form";

const NumberTextField = memo(({
    name, label, txtType, fullWidth,
    disabled, onChange, value, helperText,
    control, rules, errors, placeholder,
    textAlign, decimalScale }) => {
    const newTextField = useTextField(name, label, errors, txtType, placeholder, undefined, undefined, undefined, textAlign, helperText)

    const fnOnChange = (values) => {
        const { formattedValue, value, floatValue } = values;
        if (onChange !== undefined) {
            onChange(name, formattedValue, value, floatValue)
        }
        return { value: floatValue }
    }

    return (
        (control)
            ? <Controller
                name={name}
                control={control}
                rules={rules && rules}
                render={({
                    field: { onChange, onBlur, value, name, ref },
                    fieldState: { invalid, isTouched, isDirty, error },
                }) => (<NumberFormat
                    customInput={TextField}
                    {...newTextField}
                    errors={errors ?? errors}//revisar el helperText
                    helperText={errors ? errors?.message ?? helperText : helperText}
                    fullWidth={fullWidth}
                    disabled={disabled}
                    textAlign={textAlign}
                    inputRef={ref}
                    onValueChange={(values) => onChange(values.floatValue)}
                    name={name}
                    
                    value={value}
                    isNumericString={true}
                    thousandSeparator={true}
                    decimalScale={decimalScale}
                />)
                }
            />
            : <NumberFormat
                customInput={TextField}
                {...newTextField}
                fullWidth={fullWidth}
                disabled={disabled}
                onValueChange={fnOnChange}
                thousandSeparator
                textAlign={textAlign}
                value={value} />
    );
});

NumberTextField.propTypes = {
    name: PropTypes.string,
    label: PropTypes.string,
    errors: PropTypes.object,
    txtType: PropTypes.string,
    fullWidth: PropTypes.bool,
    disabled: PropTypes.bool,
    rules: PropTypes.object,
    helperText: PropTypes.string,
    placeholder: PropTypes.string,
    decimalScale: PropTypes.number,
};

NumberTextField.defaultProps = {
    txtType: 'text',
    fullWidth: true,
    disabled: false,
    label: ' ',
    helperText: '',
    placeholder: '',
}

export default NumberTextField;

