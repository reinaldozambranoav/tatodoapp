import { TextField, Tooltip } from '@mui/material'
import PropTypes from 'prop-types'
import { memo, useState } from "react"
import { Controller } from "react-hook-form"
import NumberFormat from "react-number-format"
import { helperTextStyles, textfieldLabelStyles, textfieldStyles } from './TextField'
import InfoIcon from '@mui/icons-material/InfoOutlined'

const TaNumberTextField = memo(({
  id = '', name = '', control, decimalScale = 2,
  label = '', value, fullWidth = true, disabled = false,
  onChange, textAlign, rules, helperText = '',
  fontSize, color, thousandSeparator = true, defaultValue,
  uniqueKey, errorIcon = false, onKeyDown
}) => {

  const [isOpenTooltip, setIsOpenTooltip] = useState(false)
  const textfieldClasses = textfieldStyles({ fontSize, color })
  const textfieldLabelClasses = textfieldLabelStyles({ color, textAlign })
  const helperTextClasses = helperTextStyles()

  return (
    <>
      {control
        ? <Controller
          name={name}
          control={control}
          rules={rules}
          render={({ field, fieldState: { error } }) => {
            return (<NumberFormat
              id={id}
              customInput={TextField}
              register={field}
              label={label}
              error={error !== undefined}
              helperText={errorIcon ? helperText : error ? error?.message ?? helperText : helperText}
              fullWidth={fullWidth}
              onKeyDown={onKeyDown}
              disabled={disabled}
              onValueChange={(values) => field.onChange(values.floatValue)}
              variant='standard'
              rules={rules}
              InputProps={{
                classes: { input: textfieldClasses.input, underline: textfieldClasses.underline },
                endAdornment: errorIcon && error !== undefined && <Tooltip open={isOpenTooltip} sx={{
                  fontFamily: 'Archivo',
                  fontSize: "11pt",
                  color: "var(--main-text-color0)"
                }} onClose={() => setIsOpenTooltip(false)} title={error?.message ?? ''}>
                  <InfoIcon
                    onMouseEnter={() => setIsOpenTooltip(preVal => !preVal)}
                    onClick={() => setIsOpenTooltip(preVal => !preVal)}
                    sx={{ color: 'var(--main-bg-color9)' }}
                  />
                </Tooltip>,
              }}
              InputLabelProps={{
                classes: { root: textfieldLabelClasses.root, focused: textfieldLabelClasses.focused, },
                shrink: true,
              }}
              FormHelperTextProps={{
                classes: { root: helperTextClasses.root, disabled: helperTextClasses.disabled }
              }}
              thousandSeparator={thousandSeparator}
              decimalScale={decimalScale}
              isNumericString={true}
              value={field.value}
              displayType={"input"}
            />)
          }}
        />
        : <NumberFormat
          id={id}
          name={name}
          customInput={TextField}
          label={label}
          helperText={helperText}
          fullWidth={fullWidth}
          disabled={disabled}
          onValueChange={(values) => onChange && onChange(name, values.floatValue, uniqueKey)}
          onKeyDown={onKeyDown}
          defaultValue={defaultValue}
          variant={'standard'}
          InputProps={{
            classes: { input: textfieldClasses.input, underline: textfieldClasses.underline },
          }}
          InputLabelProps={{
            classes: { root: textfieldLabelClasses.root, focused: textfieldLabelClasses.focused, },
            shrink: true,
          }}
          FormHelperTextProps={{
            classes: { root: helperTextClasses.root, disabled: helperTextClasses.disabled }
          }}
          thousandSeparator={thousandSeparator}
          decimalScale={decimalScale}
          isNumericString={true}
          value={value}
          displayType={"input"}
        />
      }
    </>
  )
})

export default TaNumberTextField

TaNumberTextField.propTypes = {
  name: PropTypes.string,
  label: PropTypes.string,
  error: PropTypes.object,
  fullWidth: PropTypes.bool,
  disabled: PropTypes.bool,
  rules: PropTypes.object,
  helperText: PropTypes.string,
  placeholder: PropTypes.string,
  decimalScale: PropTypes.number,
}
