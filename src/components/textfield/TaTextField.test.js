import { mount } from 'enzyme'
import TaTextField from './TaTextFieldOld'
import toJson, { mountToJson } from 'enzyme-to-json';

describe('Test suite for <TaTextField />', () => {
  let component = null

  beforeEach(() => {
    component = mount(<TaTextField name={'PROBANDO'} label={'TENGO LABEL'} />)
    // console.log(mountToJson(component)) // DE ESTA FORMA SE PINTA UN TREEVIEW EN FORMATO JSON DEL COMPONENTE
  })

  afterEach(() => {
    component = null
  })

  test('Should render the component properly', () => {
    expect(component).toMatchSnapshot()
  })

  test('Should render the label', () => {
    expect(component.exists('label')).toEqual(true)
  })

})