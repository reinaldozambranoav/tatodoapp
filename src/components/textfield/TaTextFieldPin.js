import React, { useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

import {
    TextField,
} from '@mui/material/';

import { createMuiTheme, ThemeProvider } from '@mui/styles';

import Autocomplete from '@material-ui/lab/Autocomplete';
import NumberFormat from 'react-number-format'

import {
    TaIsEmpty,
} from 'functions/DataValidation'

import ModalKeyboard from "./ModalKeyboard"

import {
    getUseKeyBoard,
} from 'constants/UserStationConfiguration'


let enterPressed = false;

const onDoubleclick = (checkKeyboard, disabled) => {
    if ((disabled === false || disabled === undefined) && getUseKeyBoard() === true) {
        checkKeyboard(true)
    }
}

const onFocus = (e) => e.target.select()

const TextFieldTheme = createMuiTheme({
    overrides: {
        MuiInputBase: {
            root: {
                marginTop: "10px",
                marginBottom: "10px",
                padding: "8px 0px 2px 0px",
                fontSize: "12pt",
                fontFamily: "Archivo",
                color: 'var(--main-text-color0)',
                height: "40px",
                backgroundColor: "var(--main-bg-color0)",
            },
            input: {
                padding: "8px 0px 2px 0px",
            },
        },
        MuiOutlinedInput: {
            root: {
                borderRadius: "30px",
                '&$focused $notchedOutline': {
                    borderColor: 'var(--main-bg-color0)',
                },
            }
        }
    },
});


const TaTextField = React.memo((
    { id, value, name,
        label, type, placeholder,
        helperText, decimalScale, style,
        fullWidth, onBlur, align,
        onChange, options, disabled,
        maxLength, isNumeric, autoFocus,
        inputRef, }
) => {
    const [val, setVal] = useState('');
    const [isOpenKeyboard, setIsOpenKeyboard] = useState(false)
    useEffect(() => {
        if (options) {
            if (value !== undefined && value !== "") {
                const currentOption = options.find(x => x.id === value)
                setVal(currentOption === undefined ? "" : currentOption.value);
            } else {
                setVal("");
            }
        } else {
            setVal(value);
        }

    }, [value, options]);

    const fnOnBlur = useCallback((event) => {
        if (onBlur) {
            onBlur(event.target.name, event.target.value, enterPressed)
            enterPressed = false;
        }
    }, [])

    const fnOnKeyDown = useCallback((event) => {
        try {
            if (event.key === 'Enter') {
                enterPressed = true;
                event.target.blur();
            }
        } catch (error) {

        }

    }, [])

    const fnSetValue = useCallback((event, event2) => {
        let currentValue = (isNumeric) ? event.floatValue : (event2) ? event2.value : event.target.value
        currentValue = (currentValue) ? currentValue : "";
        setVal(currentValue);
        if (onChange) {
            if (options) {
                try {
                    onChange(name, event2.id, event2)
                } catch (error) {
                    onChange(name, "")
                }
            } else {
                onChange(name, currentValue)
            }

        }
    }, [name, options, isNumeric])

    const onCloseKeyboard = (onClose, name, value, onEnter) => {
        if(name !== undefined && value !== undefined){
            if (onBlur) {
                onBlur(name, value, onEnter)
            }
        }
        onClose(false)
    }

    return (
        <ThemeProvider theme={TextFieldTheme}>
            <React.Fragment>
                <TextField
                    id={id}
                    value={val}
                    name={name}
                    label={label}
                    type={type}
                    placeholder={placeholder}
                    helperText={helperText}
                    fullWidth={fullWidth && fullWidth}
                    onChange={fnSetValue}
                    onKeyDown={fnOnKeyDown}
                    onBlur={fnOnBlur}
                    autoComplete={"new-password"}
                    inputProps={{ maxLength: maxLength, style: { textAlign: align } }}
                    disabled={disabled}
                    autoFocus={(autoFocus) && autoFocus}
                    style={(style) && style}
                    variant="outlined" 
                    onDoubleClick={() => onDoubleclick(setIsOpenKeyboard, (disabled) && disabled)}
                    onFocus={onFocus}
                    inputRef={(inputRef) && inputRef}/>
                <ModalKeyboard
                    open={isOpenKeyboard}
                    onClose={(name, input) => onCloseKeyboard(setIsOpenKeyboard, name, input)}
                    name={name}
                    isNumeric={true}
                    isPassword={type === 'password'}
                    onChangeTextField={fnSetValue}
                    onKeyDownTextField={(event) => fnOnKeyDown(event)}
                    currentValue={(isNumeric) ? '' : value} />
            </React.Fragment>
        </ThemeProvider>

    );
});

TaTextField.propTypes = {
    id: PropTypes.string.isRequired,
    value: PropTypes.any.isRequired,
    name: PropTypes.string,
    label: PropTypes.string,
    type: PropTypes.string,
    placeholder: PropTypes.string,
    helperText: PropTypes.string,
    decimalScale: PropTypes.number,
    style: PropTypes.object,
    align: PropTypes.string,
    options: PropTypes.array,
    onChange: PropTypes.func,
    disabled: PropTypes.bool,
    maxLength: PropTypes.string,
};

TaTextField.defaultProps = {
    name: '',
    label: '',
    type: 'password',
    placeholder: '',
    align: 'center',
    disabled: false,
    maxLength: "4",
};

export default TaTextField;