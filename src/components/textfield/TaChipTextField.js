import { Chip } from "@mui/material"
import { useState } from "react"
import { memo } from "react"
import { Controller } from "react-hook-form"
import TaTextField from "./TaTextField"

const TaChipTextField = memo(({
  id, name, label = '', control, fullWidth = true,
  placeholder = '', autoFocus = false, disabled = false,
  pattern
}) => {

  const [ helperText, setHelperText ] = useState('')
  const fnOnKeyDown = (e, field) => {
    if (e.key === "Enter") {
      const duplicatedValues = field.value.indexOf(e.target.value.trim())
      if (duplicatedValues !== -1) return e.target.value = ''
      if (!e.target.value.replace(/\s/g, "").length) return e.target.value = ''
      if (!(pattern.value).test(e.target.value)) return setHelperText(pattern.message)
      field.onChange(field.value.concat(e.target.value.trim()))
      return e.target.value = ''
    }
    setHelperText('')
  }

  return <Controller
    name={name}
    control={control}
    render={({ field }) => {
      return (<TaTextField
        id={id}
        label={label}
        fullWidth={fullWidth}
        placeholder={placeholder}
        onKeyDown={(e) => fnOnKeyDown(e, field)}
        error={helperText !== '' && helperText}
        autoFocus={autoFocus}
        disabled={disabled}
        isAdorned={true}
        adornment={field.value.map(x => (<Chip
          key={x}
          variant={'filled'}
          tabIndex={-1}
          label={x}
          sx={{ margin: '0px 2px 5px 2px' }}
          onDelete={() => field.onChange(field.value.filter(s => s !== x))}
        />))}
      />)
    }}
  />
})

export default TaChipTextField;
