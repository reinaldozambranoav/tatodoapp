import React, { memo, useState, useCallback, useEffect, } from 'react';
import PropTypes from 'prop-types';

import Grid from "@mui/material/Grid";
import Pagination from '@material-ui/lab/Pagination';

const TaPagination = memo(({ totalPages, currentPage, onChange, size, siblingPages, hidden }) => {
    const [page, setPage] = useState(currentPage)
    useEffect(() => {
        setPage(currentPage)
    }, [currentPage])

    const handleOnChange = useCallback((event, value) => (onChange) && onChange(value), [])
    return (
        <Grid container alignContent="center" alignItems="center">
            <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align="center">
                {!(hidden) && <Pagination style={{ display: 'inline-flex' }}
                    count={totalPages} page={page} onChange={handleOnChange} size={size} shape="rounded" siblingCount={siblingPages} />}
            </Grid>
        </Grid>

    )
});

TaPagination.propTypes = {
    totalPages: PropTypes.number,
    currentPage: PropTypes.number,
    onChange: PropTypes.func,
    size: PropTypes.string,
    siblingPages: PropTypes.number
};

TaPagination.defaultProps = {
    currentPage: 1,
    totalPages: 10,
    size: "medium",
    siblingPages: 0,
    hidden: false,
}

export default TaPagination;