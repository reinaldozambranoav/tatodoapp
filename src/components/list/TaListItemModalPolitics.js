import { Grid } from '@mui/material'
import TaCheckBox from 'components/checkbox/TaCheckBox'
import TaListItem from './TaListItem'

const TaListItemModalPolitics = (({ name, active, fnOnChange, uniqueKey, disabled }) => {

    return (
        <TaListItem key={uniqueKey}>
            <Grid container spacing={2} direction="row" justifyContent="space-evenly" alignItems="center" >
                <Grid item xs={12} sm={12} md={9} lg={9} xl={9}>
                    {`${name}`}
                </Grid>
                <Grid item xs={12} sm={12} md={3} lg={3} xl={3} align={'center'}>
                    <TaCheckBox status={active} onChange={fnOnChange} uniqueKey={uniqueKey} disabled={disabled} />
                </Grid>
            </Grid>
        </TaListItem>

    )
})

export default TaListItemModalPolitics
