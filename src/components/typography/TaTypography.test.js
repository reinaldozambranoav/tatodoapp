import renderer from 'react-test-renderer'
import TaTypography from './TaTypography';


test('component test Typography',() => {
    const TaTypography1 = renderer.create(
        <TaTypography text={'Esta es mi tipografia'} />
    )
// 
    let tree = TaTypography1.toJSON();
    //console.log(tree)
    
     let text
     if(tree.children !== null){
        text = tree.children[0]
     }
    expect(text).toBeDefined()
    expect.stringContaining(text)
})
// describe('Component Test de TaTypography',()=>{
//     let component
//     beforeEach(()=>{
//         component= shallow(<TaTypography text={'Esta es mi tipografia'}/>)
//     })
//     console.log(component.find.toMat())



// })
