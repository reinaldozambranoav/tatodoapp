import React, { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import TaTypography from "../typography/TaTypography"

const Cooldown = memo(({ iniValue, message, onStop }) => {

  const [currentValue, setCurrentValue] = useState(-1);

  useEffect(() => {
    setCurrentValue(iniValue);
  }, [iniValue]);

  useEffect(() => {
    if (currentValue > 0) {
      var timeOut = setTimeout(() => {
        const newValue = currentValue - 1;
        setCurrentValue(newValue);
      }, 1000);
      return () => {
        clearTimeout(timeOut);
      }
    } else if (currentValue === 0) {
      onStop();
    }
  })
  
  return currentValue > 0 && <TaTypography>{`${currentValue} ${message}`}</TaTypography>;
});

Cooldown.propTypes = {
  iniValue: PropTypes.number,
  message: PropTypes.object,
};

export default Cooldown;