import React from 'react'
import SvgIcon from '@mui/material/SvgIcon'

const EditFavoriteA = () => {
    return (
        <SvgIcon viewBox="-1 -2 11 11">
            <path 
                className="st0" 
                d="M3,0.3L3.9,2l1.9,0.3L4.4,3.7l0.3,1.9L3,4.7L1.3,5.6l0.3-1.9L0.3,2.3L2.2,2L3,0.3z"/>
        </SvgIcon>
    )
}

export default EditFavoriteA