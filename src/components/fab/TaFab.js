import { useEffect, useState, memo, Fragment } from 'react'
import PropTypes from 'prop-types'
import { makeStyles } from '@mui/styles'
import Fab from '@mui/material/Fab'
import Tooltip from '@mui/material/Tooltip'
import TaTypography from '../typography/TaTypography'
import { iconType } from './Icons'

const TaFabStyle = makeStyles({
  fabRoot: {
    width: props => props.miniFab  ? '30px' : undefined,
    height: props => props.miniFab  ? '30px' : undefined,
    minHeight: props => props.miniFab  ? '30px' : undefined
  },
  circular: {
    color: props => props.colorIcon,
    margin: props => (props.margin) ? '5px' : '0px',
    '&:hover': {
      backgroundColor: "var(--main-bg-color3)"
    },
    fontSize: "12pt",
    fontFamily: "Archivo",
    minWidth: props => props.extended && "125px",
    backgroundColor: props => props.color,
  },
  extended: {
    height: '40px !important',
    color: props => props.colorIcon,
    margin: props => (props.margin) ? '5px' : '0px',
    '&:hover': {
      backgroundColor: "var(--main-bg-color3)"
    },
    fontSize: "12pt",
    fontFamily: "Archivo",
    minWidth: props => props.extended && "125px",
    backgroundColor: props => props.color,
  }
})

const TaFabColor = (color) => {
  if (color === 'orange') {
    return "var(--main-bg-color3)"
  } else if (color === 'red') {
    return "red"
  } else if (color === 'green') {
    return "var(--main-bg-color5)"
  } else if (color === 'transparent') {
    return "transparent"
  } else if (color === 'white') {
    return "var(--main-bg-color0)"
  } else {
    return "var(--main-bg-color4)"
  }
}

const fnReturnLabel = (label, textButton, icon, miniFab) => {
  return (textButton) ? <Fragment>
    {iconType(icon, miniFab)} {textButton}
  </Fragment>
    : label
}

const TaFab = memo(({ id, icon, size, disabled,
  onClick, textButton, color, noMargin,
  tooltip, index, download, uniqueKey,
  colorIcon, hotkey, type, hidden, miniFab = false }) => {

  const [label, setLabel] = useState("")
  const [posLabel] = useState(0)

  useEffect(() => {
    setLabel(textButton === '' ? iconType(icon, miniFab) : textButton)
  }, [textButton, icon])

  useEffect(() => {
    if (hotkey === "") {
      setLabel(textButton === '' ? iconType(icon, miniFab) : textButton)
    }
  }, [hotkey, textButton, icon])

  const fabColor = TaFabColor(color)
  const classes = TaFabStyle({ color: fabColor, margin: !(noMargin), colorIcon: colorIcon, extended: (textButton), miniFab})
  const finalLabel = fnReturnLabel(label, textButton, icon, miniFab)
  return (
    !(hidden) ? <Tooltip arrow title={tooltip !== "" ? <TaTypography color="var(--main-text-color1)">{tooltip}</TaTypography> : ""} aria-label={tooltip}>
      <Fab
        id={id}
        variant={(textButton) ? 'extended' : 'circular'}
        aria-label={icon}
        data-id={index}
        download={(download) && download}
        size={(textButton) ? 'large' : size}
        disabled={disabled}
        classes={{ circular: classes.circular, extended: classes.extended, root: classes.fabRoot }}
        onClick={(onClick !== undefined) ? (event) => onClick(id, uniqueKey, event, index) : null}
        type={(type) && type}>
        {posLabel === 0 ? finalLabel : hotkey === "" ? finalLabel : hotkey}
      </Fab>
    </Tooltip>
      : null
  )
})

TaFab.propTypes = {
  id: PropTypes.string,
  icon: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  size: PropTypes.string,
  topMargin: PropTypes.bool,
  floatLeft: PropTypes.bool,
  tooltip: PropTypes.string,
  textButton: PropTypes.string,
  color: PropTypes.string,
  hotkey: PropTypes.string,
}

TaFab.defaultProps = {
  disabled: false,
  size: "small",
  floatLeft: false,
  tooltip: '',
  textButton: '',
  color: "",
  colorIcon: "var(--main-bg-color0)",
  hotkey: "",
}

export default TaFab