import Print from '@mui/icons-material/Print';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import Toc from '@mui/icons-material/Toc';
import Key from '@mui/icons-material/VpnKey';
import RestoreIcon from '@mui/icons-material/Restore';
import CollectionsIcon from '@mui/icons-material/Collections';
import GridOnIcon from '@mui/icons-material/GridOn';
import LibraryAddIcon from '@mui/icons-material/LibraryAdd';
import Sync from '@mui/icons-material/Sync';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import NotInterestedIcon from '@mui/icons-material/NotInterested';
import DeleteSweepIcon from '@mui/icons-material/DeleteSweep';
import AddLocation from '@mui/icons-material/AddLocation';
import GridOn from '@mui/icons-material/GridOn';
import Receipt from '@mui/icons-material/Receipt';
import MergeType from '@mui/icons-material/MergeType';
import CallMade from '@mui/icons-material/CallMade';
import OpenInNew from '@mui/icons-material/OpenInNew';
import LockOpen from '@mui/icons-material/LockOpen';
import NotInterested from '@mui/icons-material/NotInterested';
import Message from '@mui/icons-material/Message';
import Telegram from "@mui/icons-material/Telegram";
import MoneyIcon from '@mui/icons-material/Money';
import CommentIcon from '@mui/icons-material/Comment';
import File from '@mui/icons-material/Description'
import SettingsBackupRestoreIcon from '@mui/icons-material/SettingsBackupRestore';
import ArrowDownwardIcon from '@mui/icons-material/ArrowDownward';
import ArrowUpwardIcon from '@mui/icons-material/ArrowUpward';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import InfoIcon from '@mui/icons-material/Info';
import MenuIcon from '@mui/icons-material/Dehaze';
import RemoveIcon from '@mui/icons-material/Remove';
import SaveIcon from '@mui/icons-material/Save';
import SearchIcon from '@mui/icons-material/Search';
import SendIcon from '@mui/icons-material/Send';
import SettingsIcon from '@mui/icons-material/SettingsSharp';
import Storage from '@mui/icons-material/Storage';
import ArrowIcon from '@mui/icons-material/KeyboardArrowRight';
import Totalize from '@mui/icons-material/Done';
import Home from '@mui/icons-material/Home';
import Upload from '@mui/icons-material/Unarchive';
import CashIcon from '@mui/icons-material/MonetizationOn';
import CardIcon from '@mui/icons-material/CreditCard';
import List from '@mui/icons-material/ListAlt';
import Star from '@mui/icons-material/Star';
import FileCopy from '@mui/icons-material/FileCopy';
import SystemUpdate from '@mui/icons-material/SystemUpdate';
import CloudDownload from '@mui/icons-material/CloudDownload';
import StarBorderOutlined from '@mui/icons-material/StarBorderOutlined';
import AccountCircle from '@mui/icons-material/AccountCircle';
import PhotoCamera from '@mui/icons-material/PhotoCamera';
import AddIcon from '@mui/icons-material/Add';
import ClearIcon from '@mui/icons-material/Clear';
import DeleteIcon from '@mui/icons-material/Delete';
import DoneIcon from '@mui/icons-material/Done';
import EditIcon from '@mui/icons-material/Edit';
import { ClearAll } from '@mui/icons-material';
//COMPONENTS
import EraseIcon from './icons/EraseIcon'
import EditImageIcon from './icons/EditImageIcon'
import ImageIcon from './icons/ImagesIcon'
import PercentIcon from './icons/PercentIcon'
import PriceVisorIcon from './icons/PriceVisorIcon'
import ExactPayment from './icons/ExactPayment'
import BarcodeScan from './icons/BarcodeScan'
import FE from './icons/FE'
import OneTileFilter from './icons/OneTileFilter'
import FourTileFilter from './icons/FourTileFilter'
import MultiTileFilter from './icons/MultiTileFilter'
import AccountCash from './icons/AccountCash'
import PrinterPos from './icons/PrinterPos'
import DownloadDoc from './icons/DownloadDocument'

export const iconType = (type, miniFab = false) => {
    switch (type) {
        case "add":
            return <AddIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "backArrow":
            return <ArrowBackIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "forwardArrow":
            return <ArrowForwardIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "clear":
        case "exit":
            return <ClearIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "delete":
            return <DeleteIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "done":
            return <DoneIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "edit":
            return <EditIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "erase":
            return <EraseIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "info":
            return <InfoIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "menu":
            return <MenuIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "percent":
            return <PercentIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "pricevisor":
            return <PriceVisorIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "remove":
            return <RemoveIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "save":
            return <SaveIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "search":
            return <SearchIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "setting":
            return <SettingsIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "send":
            return <SendIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "storage":
            return <Storage sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "arrow":
        case "totalarrow":
            return <ArrowIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "total":
        case "check":
            return <Totalize sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "home":
            return <Home sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "upload":
            return <Upload sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "cash":
            return <CashIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "card":
            return <CardIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "list":
            return <List sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "favorite":
            return <Star sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "favoriteB":
        case "editFavsSelected":
            return <StarBorderOutlined sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "image":
            return <ImageIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "editImage":
            return <EditImageIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "editImageSelected":
            return <EditImageIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "systemUpdate":
            return <SystemUpdate sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "cloudDownload":
            return <CloudDownload sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "exactPayment":
            return <ExactPayment sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "FileCopy":
            return <FileCopy sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "camera":
            return <PhotoCamera sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'user':
            return <AccountCircle sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'disableUser':
            return <AccountCircle sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'printer':
            return <Print sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'barcodeScan':
            return <BarcodeScan sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case '...':
            return <MoreVertIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'visibility':
            return <Visibility sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'visibilityOff':
            return <VisibilityOff sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'oneTileFilter':
            return <OneTileFilter sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'fourTileFilter':
            return <FourTileFilter sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'multiTileFilter':
            return <MultiTileFilter sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'multiSelect':
            return <Toc sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'key':
            return <Key sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'FE':
            return <FE sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'accountCash':
            return <AccountCash sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'printerPos':
            return <PrinterPos sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'restore':
            return <RestoreIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'mapType':
            return <CollectionsIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'listMode':
            return <GridOnIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'mergeTables':
            return <LibraryAddIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'clearTable':
            return <DeleteSweepIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'sync':
            return <Sync sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'disable':
            return <NotInterestedIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'downloadDoc':
            return <DownloadDoc sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'addLocation':
            return <AddLocation sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'polygon':
            return <GridOn sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "merge":
            return <MergeType sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "document":
            return <Receipt sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "moveDoc":
            return <CallMade sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "openTable":
            return <OpenInNew sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "freeTable":
        case "lockOpen":
            return <LockOpen sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "cancel":
            return <NotInterested sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "message":
            return <Message sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case "telegram":
            return <Telegram sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'money':
            return <MoneyIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'comment':
            return <CommentIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'file':
            return <File sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'backupRestore':
            return <SettingsBackupRestoreIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'arrowUp':
            return <ArrowUpwardIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'arrowDown':
            return <ArrowDownwardIcon sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        case 'clearAll':
            return <ClearAll sx={miniFab ? { width: '20px', height: '20px' } : {}} />
        default:
            return null
    }
}