import { Paper, Tab, Tabs, Typography } from "@mui/material"
import { memo, forwardRef, useImperativeHandle, useState, useCallback, useMemo, useEffect } from "react"
import { v4 as uuid } from 'uuid'
import { tabBodyStyles, tabLabelStyles } from "./TaTabsStyles"


const TaTabs = memo(forwardRef(({
  tabIndex, children, labelArray, customBackGround,
  customSelectedBackGround, customSelectedColor, customUnactiveBackground, customUnactiveColor,
  disabled, customTabHeight, customHeight,
},ref) => {

  const [ currentTab, setCurrentTab ] = useState(0)


  const fnSetCurrentTab = (value) => setCurrentTab(value)

  useImperativeHandle(ref, () => ({
    activeTabs: fnSetCurrentTab,
  }), [ labelArray, currentTab ])



  const fnOnChange = useCallback((event, value) => setCurrentTab(value), []);

  const tabLabelClasses = tabLabelStyles({
    customUnactiveColor, customBackGround, customUnactiveBackground,
    customSelectedColor, customSelectedBackGround, customTabHeight
  })

  const tabBodyClasses = tabBodyStyles({ customBackGround })

  const currentLabel = useMemo(() => {
    const newLabelArray = labelArray.map(x => ({
      id: uuid(),
      label: (x.label) && x.label,
      icon: (x.icon) && x.icon,
      hidden: (x.hidden) && x.hidden
    }))
    return newLabelArray.filter(x => (x.hidden === false || x.hidden === undefined)).map(x => (<Tab
      key={`tabLabel:${x.id}`}
      classes={tabLabelClasses}
      disabled={disabled}
      label={<Typography sx={{ zIndex: 4 }} align={'center'} >{x.label}</Typography>} icon={x.icon} />))
  }, [ labelArray, disabled ])

  useEffect(() => (tabIndex !== undefined) && (tabIndex !== currentTab) && setCurrentTab(tabIndex)[ tabIndex ])

  return <>
    <Tabs
      id={`TaTabs`}
      value={currentTab}
      classes={tabBodyClasses}
      onChange={fnOnChange}
      variant='fullWidth'>
      {currentLabel}
    </Tabs>
    <Paper id={'tabContentPaper'} elevation={0} square={true} sx={{ padding: '5px', height: customHeight }}>
      {children[ currentTab ]}
    </Paper>
  </>
}))

export default TaTabs