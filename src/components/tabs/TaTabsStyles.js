import { makeStyles } from '@mui/styles'

export const tabBodyStyles = makeStyles(() => ({
    root: {
      backgroundColor: props => props.customBackGround ? props.customBackGround : 'var(--main-bg-color0)',
      // height: props => props?.customTabHeight ?? '63px'
      // padding: '15px 0px 0px 0px'
    },
    scroller: {
      overflow: 'visible!important',
    },
    indicator: {
      display: 'none',
    },
  }))
  export const tabLabelStyles = makeStyles(() => ({
    root: {
      opacity: 1,
      overflow: 'initial',
      color: props => props.customUnactiveColor ? props.customUnactiveColor : "var(--main-bg-color0)",
      background: props => props.customBackGround ? props.customBackGround : 'rbga(0,0,0,0)',
      transition: '0.1s',
      fontFamily: "Archivo",
      fontSize: "14pt",
      flexGrow: 1,
      padding: "2px",
      height: props => props.customTabHeight,
      paddingTop: "10px",
      '&:before': {
        content: '" "',
        position: 'absolute',
        bottom: 0,
        right: 0,
        left: 0,
        paddingTop: '5px',
        borderTopLeftRadius: '5px',
        borderTopRightRadius: '5px',
        height: '80%',
        boxShadow: '3px 3px 8px 0 rgba(0,0,0,0.38) !important',
        backgroundColor: props => props.customUnactiveBackground ? props.customUnactiveBackground : 'var(--main-bg-color8)',
      },
      '&:after': {
        pointerEvents: 'none',
        transition: '0.2s',
        content: '" "',
        position: 'absolute',
        bottom: 0,
        right: 0,
        transform: 'translateX(100%)',
        paddingTop: '5px',
        borderTopLeftRadius: '5px',
        borderTopRightRadius: '5px',
        borderBottomRightRadius: '-5px',
        boxShadow: '3px 3px 8px 0 rgba(0,0,0,0.38) !important',
        height: '80%',
        display: 'block',
      },
    },
    selected: {
      color: props => props.customSelectedColor ? props.customSelectedColor : 'var(--main-bg-color8) !important',
      borderTopLeftRadius: '5px',
      borderTopRightRadius: '5px',
      borderBottomRightRadius: '-5px',
      paddingTop: "10px",
      '&:before': {
        backgroundColor: props => props.customSelectedBackGround ? props.customSelectedBackGround : 'var(--main-bg-color0) !important',
        boxShadow: '3px 3px 8px 0 rgba(0,0,0,0.38) !important',
        height: '87% !important',
        borderTopLeftRadius: '5px',
        borderTopRightRadius: '5px',
        borderBottomRightRadius: '-5px',
      },
      '&:after': {
        height: '87% !important',
      },
    },
    wrapper: {
      paddingTop: "10px",
      textTransform: 'initial',
    },
    flexContainer: {
      height: '63px !important',
    }
  }))