
import { makeStyles } from '@mui/styles';

export default makeStyles({
    listBox: {
        fontSize: "14pt",
        fontFamily: "Archivo",
        color: props => props.color !== 'var(--main-bg-color0)' ?'var(--main-bg-color1)' : 'var(--main-bg-color0)',
        backgroundColor: props => props.color === 'var(--main-bg-color0) !important'?'var(--main-bg-color1)' :'var(--main-bg-color0)'
    }
});