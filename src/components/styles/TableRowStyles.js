import { makeStyles } from '@mui/styles';

export default makeStyles({
    selected: {
        backgroundColor: "var(--main-list-color0) !important",
    }
})