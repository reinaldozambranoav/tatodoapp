import { Grid, ListItemButton, Paper } from "@mui/material"
import TaImg from "components/img/TaImg"
import { memo } from "react"
import useWindowSize from '../../helper/UseWindowsSize'

const GridImgDetail = memo(({ itemData, onClick, onKeyDown, classes: { layoutImage } }) => {

  // console.log('layoutImage', layoutImage)
  // console.log(itemData)
  const { height, width } = useWindowSize()

  const stylesGridDetailImage = {
    layoutImage: {
      borderRadius: "5px",
      borderStyle: "solid",
      borderColor: "var(--main-bg-color1)",
      cursor: 'pointer',
      fontFamily: 'Archivo',
      fontSize: "14pt",
      color: "var(--main-text-color0)",
      overflow: 'hidden',
      width: '100%',
      height: height - 390
    },
  }
  return (
    <ListItemButton
      id={`GridItem:${itemData.uniqueKey}`}
      sx={{ padding: '0px' }}
      // classes={{ root: layout }}
      onClick={() => onClick(itemData)}
      onKeyDown={(e) => onKeyDown(e, itemData)}>
      <Paper elevation={0} square={true} classes={{ root: layoutImage }}>
        <Grid container direction="column" justifyContent="center" alignItems="center">
          <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
            <TaImg url={itemData.url} inStyle={{ height: height - 390, marginLeft: 'auto', marginRight: 'auto' }} />
          </Grid>
        </Grid>
      </Paper>
    </ListItemButton>

  )
})

export default GridImgDetail