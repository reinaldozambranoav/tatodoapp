import { Grid, Pagination, PaginationItem, Paper } from '@mui/material'
import usePagination from 'helper/usePagination'
import GridImgDetail from './GridImgDetail'
import { forwardRef, memo, useEffect, useImperativeHandle, useState } from 'react'
import GridItemDetail from './GridItemDetail'
import GridCategoyDetail from './GridCategoyDetail'
import GridPaymentMethodsDetail from './GridPaymentMethodsDetail'
import useWindowSize from 'helper/UseWindowsSize'
import { fnCalculateHeights } from 'helper/fnCalculateHeights'
import { gridListConfigModel, gridListHeightModel, gridListStyles } from './GridListModel'

const TaGridList = memo(forwardRef(({ onCallback, data, gridType, showImage = false, height }, ref) => {

  const [{ selectedKey, currentCol, gridHeight, qtyPerPage, gridItemHeight, infoHeight }, setGridListConfig] = useState(gridListConfigModel())

  const { paginatedData, fnOnChangePage, pageQty, currentPage } = usePagination(data, qtyPerPage)

  const { width, height: sizeHeight } = useWindowSize()

  const classes = gridListStyles(gridListHeightModel(gridHeight, height, sizeHeight))

  const fnOnChange = (e, value) => fnOnChangePage(value)

  const fnOnClick = (data) => {
    setGridListConfig(preVal => ({ ...preVal, selectedKey: data.uniqueKey }))
    if (onCallback !== undefined) onCallback(data, gridType)
  }
  const fnOnFocus = (data) => setGridListConfig(preVal => ({ ...preVal, selectedKey: data.uniqueKey }))

  useImperativeHandle(ref, () => ({
    setGridListPage: fnOnChangePage
  }), [])

  const fnOnKeyDown = (event, data) => {
    switch (event.key) {
      case 'Enter':
        fnOnClick(data)
        break
      //AGREGAR MAS CASOS PARA LA NAVEGACIÓN CON LAS FLECHAS DIRECCIONALES
      default:
        break
    }
  }

  useEffect(() => {
    let currentGridItemHeight = fnCalculateHeights([height, 60])
    let qtyPerPage, newCol, padMar = 20, gridTileHeight;
    // fnOnChangePage(1)
    if (gridType === 'items') {
      if (showImage) {
        if (width >= 1920 && currentCol !== 6) {
          gridTileHeight = 140;
          newCol = 6;
          qtyPerPage = Math.floor(((+(currentGridItemHeight.replace("px", ""))) / (gridTileHeight + padMar))) * newCol
          setGridListConfig(preVal => gridListConfigModel({ ...preVal, gridHeight: gridTileHeight, currentCol: Math.floor(12 / newCol), qtyPerPage: qtyPerPage }))
        } else if (width < 1920 && width >= 1280 && currentCol !== 5) {
          gridTileHeight = 130;
          newCol = 5;
          qtyPerPage = Math.floor(((+(currentGridItemHeight.replace("px", ""))) / (gridTileHeight + padMar))) * newCol
          setGridListConfig(preVal => gridListConfigModel({ ...preVal, gridHeight: gridTileHeight, currentCol: Math.floor(12 / newCol), qtyPerPage: qtyPerPage }))
        } else if (width < 1280 && currentCol !== 4) {
          gridTileHeight = 120;
          newCol = 4;
          qtyPerPage = Math.floor(((+(currentGridItemHeight.replace("px", ""))) / (gridTileHeight + padMar))) * newCol
          setGridListConfig(preVal => gridListConfigModel({ ...preVal, gridHeight: gridTileHeight, currentCol: Math.floor(12 / newCol), qtyPerPage: qtyPerPage }))
        }
      } else {
        gridTileHeight = 120;
        if (width >= 1920 && currentCol !== 4) {
          newCol = 4;
          qtyPerPage = Math.floor(((+(currentGridItemHeight.replace("px", ""))) / (gridTileHeight + padMar))) * newCol
          setGridListConfig(preVal => gridListConfigModel({ ...preVal, gridHeight: gridTileHeight, currentCol: Math.floor(12 / newCol), qtyPerPage: qtyPerPage }))
        } else if (width < 1920 && width >= 1280 && currentCol !== 3) {
          newCol = 3;
          qtyPerPage = Math.floor(((+(currentGridItemHeight.replace("px", ""))) / (gridTileHeight + padMar))) * newCol
          setGridListConfig(preVal => gridListConfigModel({ ...preVal, gridHeight: gridTileHeight, currentCol: Math.floor(12 / newCol), qtyPerPage: qtyPerPage }))
        } else if (width < 1280 && currentCol !== 2) {
          newCol = 2;
          qtyPerPage = Math.floor(((+(currentGridItemHeight.replace("px", ""))) / (gridTileHeight + padMar))) * newCol
          setGridListConfig(preVal => gridListConfigModel({ ...preVal, gridHeight: gridTileHeight, currentCol: Math.floor(12 / newCol), qtyPerPage: qtyPerPage }))
        }
      }
    } else if (gridType === 'category' || gridType === 'paymentMethods') {
      gridTileHeight = showImage ? 120 : 50;
      newCol = 2;
      qtyPerPage = Math.floor(((+(currentGridItemHeight.replace("px", ""))) / (gridTileHeight + padMar))) * newCol
      setGridListConfig(preVal => gridListConfigModel({ ...preVal, gridHeight: gridTileHeight, currentCol: Math.floor(12 / newCol), qtyPerPage: qtyPerPage }))
    } else if (gridType === 2) {
      gridTileHeight = showImage ? 120 : 50;
      newCol = Math.floor(12 / 2);
      qtyPerPage = Math.floor(((+(currentGridItemHeight.replace("px", ""))) / (gridTileHeight + padMar))) * newCol
      setGridListConfig(preVal => gridListConfigModel({ ...preVal, gridHeight: gridTileHeight, currentCol: Math.floor(12 / newCol), qtyPerPage: qtyPerPage }))
    } else if (gridType === 3) {
      // dispatch(setInfoView(1, 240, 1, currentGridItemHeight))
    } else if (gridType === 4) {
      // dispatch(setInfoView(3, 240, 9, currentGridItemHeight))
    } else if (gridType === 5) {
      // dispatch(setInfoView(1, showImage ? 140 : 120, 5, currentGridItemHeight))
    } else if (gridType === 'image') {
      if (width >= 1920 && currentCol !== 6) {
        gridTileHeight = 150;
        newCol = 1;
        qtyPerPage = Math.floor(((+(currentGridItemHeight.replace("px", ""))) / (gridTileHeight + padMar))) * newCol
        setGridListConfig(preVal => gridListConfigModel({ ...preVal, gridHeight: gridTileHeight, currentCol: Math.floor(12 / newCol), qtyPerPage: qtyPerPage }))
      } else if (width < 1920 && width >= 1280 && currentCol !== 5) {
        gridTileHeight = 150;
        newCol = 1;
        qtyPerPage = Math.floor(((+(currentGridItemHeight.replace("px", ""))) / (gridTileHeight + padMar))) * newCol
        setGridListConfig(preVal => gridListConfigModel({ ...preVal, gridHeight: gridTileHeight, currentCol: Math.floor(12 / newCol), qtyPerPage: qtyPerPage }))
      } else if (width < 1280 && currentCol !== 4) {
        gridTileHeight = 150;
        newCol = 1;
        qtyPerPage = Math.floor(((+(currentGridItemHeight.replace("px", ""))) / (gridTileHeight + padMar))) * newCol
        setGridListConfig(preVal => gridListConfigModel({ ...preVal, gridHeight: gridTileHeight, currentCol: Math.floor(12 / newCol), qtyPerPage: qtyPerPage }))
      }
    }
  }, [width, currentCol, data])

  useEffect(() => {
    if (pageQty === 1) fnOnChangePage(1)
  }, [pageQty])

  return (
    <Paper
      elevation={0}
      square={true}
      sx={{ backgroundColor: 'transparent', padding: '10px' }}>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12} md={12} lg={12} xl={12} >
          <Paper elevation={0} square={true} id={'paperGridList'} sx={{ overflow: 'auto', height: fnCalculateHeights([height, 60]) }}>
            <Grid container alignItems={'flex-start'} spacing={1}>
              {paginatedData.map(dataUnit => <Grid key={`item => ${dataUnit.uniqueKey}-${dataUnit.name}`} item xs={currentCol} sm={currentCol} md={currentCol} lg={currentCol} xl={currentCol}>
                {gridType === 'items' && <GridItemDetail
                  itemData={dataUnit}
                  classes={classes}
                  onFocus={fnOnFocus}
                  onClick={fnOnClick}
                  showImage={showImage}
                  onKeyDown={fnOnKeyDown}
                  selectedKey={selectedKey} />}
                {gridType === 'category' && <GridCategoyDetail
                  itemData={dataUnit}
                  classes={classes}
                  onClick={fnOnClick}
                  showImage={showImage}
                  onKeyDown={fnOnKeyDown} />}
                {gridType === 'paymentMethods' && <GridPaymentMethodsDetail
                  classes={classes}
                  paymentMethodData={dataUnit}
                  onClick={fnOnClick}
                  onKeyDown={fnOnKeyDown}
                  showImage={showImage} />}
                {gridType === 'image' && <GridImgDetail
                  itemData={dataUnit}
                  classes={classes}
                  onClick={fnOnClick}
                  onKeyDown={fnOnKeyDown} />}
              </Grid >)}
            </Grid >
          </Paper>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={12} xl={12} sx={{ height: "40px" }} >
          {pageQty !== 0 && <Pagination
            count={pageQty}
            classes={{ root: classes.pagination }}
            page={currentPage}
            onChange={fnOnChange}
            boundaryCount={1}
            siblingCount={1}
            defaultPage={1}
            showFirstButton={gridType !== 'image'}
            showLastButton={gridType !== 'image'}
            size={'small'}
            renderItem={(itemData) => <PaginationItem {...itemData} sx={{
              fontFamily: 'Archivo',
              fontSize: "11pt",
              color: "var(--main-text-color0)",
            }} />} />}
        </Grid>
      </Grid >
    </Paper >
  )
}))

export default TaGridList