import { Grid, ListItemButton, Paper } from '@mui/material'
import TaBadge from 'components/badge/TaBadge'
import TaTypography from 'components/typography/TaTypography'
import { useSalesState } from 'contexts/Sales/SalesContext'
import { memo } from 'react'

const GridCategoyDetail = memo(({ classes: { layout, content, footer },
  onClick, itemData, onKeyDown, showImage = false }) => {
  const { name, uniqueKey } = itemData
  const [{ isConfigImage }, dispatch] = useSalesState()
  return (
    <ListItemButton
      id={`GridItem:${uniqueKey}`}
      sx={{ padding: '0px' }}
      onClick={() => onClick(itemData)}
      onKeyDown={(e) => onKeyDown(e, itemData)}>
      <Paper elevation={0} square={true} classes={{ root: layout }}>
        <Grid container >
          {showImage && <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
            <Paper elevation={0} square={true} classes={{ root: content }}>
              <img src={''} alt={''} />
            </Paper>
          </Grid>}
          <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
              <Paper elevation={0} square={true} classes={{ root: footer }}>
                <Grid container direction="row" justifyContent="center" alignItems="center">
                  {name.split(' ').length > 1
                    ? name.split(' ').map(text => {
                      let gridSize = text.length > 5 ? 12 : 4
                      return <Grid key={`textKey: ${text}`} item xs={12} sm={gridSize} md={gridSize} lg={gridSize} xl={gridSize} align={'center'}>
                        <TaTypography text={text} fontSize={"10pt"} color={"var(--main-text-color1)"} fontFamily="ArchivoBold" />
                      </Grid>
                    })
                    : <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align={'center'} >
                      <TaTypography text={name} fontSize={"10pt"} color={"var(--main-text-color1)"} fontFamily="ArchivoBold" />
                    </Grid>}
                </Grid>
              </Paper>
          </Grid>
        </Grid>
      </Paper>
    </ListItemButton>
  )
})

export default GridCategoyDetail