import { makeStyles } from "@mui/styles"
import { fnCalculateHeights } from "helper/fnCalculateHeights"


export const gridListStyles = makeStyles({
  layout: {
    borderRadius: "5px",
    borderStyle: "solid",
    borderColor: "var(--main-bg-color1)",
    cursor: 'pointer',
    fontFamily: 'Archivo',
    fontSize: "14pt",
    color: "var(--main-text-color0)",
    overflow: 'hidden',
    width: '100%',
    height: props => props.gridTileHeight
  },
  layoutSelected: {
    borderRadius: "5px",
    borderStyle: "solid",
    borderColor: "var(--main-bg-color3) !important",
    cursor: 'pointer',
    fontFamily: 'Archivo',
    fontSize: "14pt",
    color: "var(--main-text-color1) !important",
    overflow: 'hidden',
    width: '100%',
    height: props => props.gridTileHeight
  },
  content: {
    overflow: 'auto',
    padding: '0px 5px 0px 5px'
  },
  container: {
    height: props => props.gridContainerHeight
  },
  footer: {
    padding: '5px',
    backgroundColor: 'var(--main-bg-color11)',
    height: '50px'
    // height: props => props.footerHeight,
  },
  outlined: {
    fontFamily: 'Archivo',
    fontSize: "14pt",
    color: "var(--main-text-color0)",
  },
  pagination: {
    "& > *": {
      margin: '5px 0px, 5px, 0px',
      justifyContent: "center",
      display: 'flex'
    },
  },
  layoutImage: {
    borderRadius: "5px",
    borderStyle: "solid",
    borderColor: "var(--main-bg-color1)",
    cursor: 'pointer',
    fontFamily: 'Archivo',
    fontSize: "14pt",
    color: "var(--main-text-color0)",
    overflow: 'hidden',
    width: '95%',
    // justifyContent: "center !important",
    height: props => props.heightImage
  },
})

export const gridListHeightModel = (gridTileHeight, gridContainerHeight, height) =>({ //SE MUERE! XDDD
  gridTileHeight: gridTileHeight,
  gridContainerHeight: fnCalculateHeights([gridContainerHeight, 60]),
  heightImage: fnCalculateHeights([height, 390]),
  // layoutHeight: fnCalculateHeights([height, 537]),
  // headerHeight: fnCalculateHeights([height, 633]),
  // contentHeight: fnCalculateHeights([height, 582]),
  // footerHeight: fnCalculateHeights([height, 636]),
  // imageHeight: fnCalculateHeights([height, 395])
})

export const gridListConfigModel = (data) => ({
  selectedKey: data?.selectedKey ?? '',
  currentCol: data?.currentCol ?? 0,
  gridHeight: data?.gridHeight ?? 170,
  qtyPerPage: data?.qtyPerPage ?? 0,
  gridItemHeight: data?.gridItemHeight ?? '',
  infoHeight: data?.infoHeight ?? 140
})
