import { Grid, ListItemButton, Paper } from '@mui/material'
import TaTypography from 'components/typography/TaTypography'
import { memo } from 'react'
import StarBorderIcon from '@mui/icons-material/StarBorder'
import StarIcon from '@mui/icons-material/Star';
import { fnFormatNumb } from 'helper/FormatNumb'

const GridItemDetail = memo(({ classes: { layoutSelected, layout, header, content, footer },
  itemData, onFocus, onClick, onKeyDown, showImage }) => {
  const { uniqueKey, selectedKey, itemName, isFav, currentPrice, descrip2, descrip3, itemId, iSpot, isUnit, exist, existU } = itemData
  return (<ListItemButton
    id={`GridItem:${uniqueKey}`}
    sx={{ padding: '0px' }}
    onFocus={() => onFocus(itemData)}
    onClick={() => onClick(itemData)}
    onKeyDown={(e) => onKeyDown(e, itemData)}>
    {showImage === false ?
      <Paper elevation={0} square={true} classes={{ root: (selectedKey === uniqueKey) ? layoutSelected : layout }}>
        <Grid container>
          <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align={'right'} >
            <Paper elevation={0} square={true} classes={{ root: header }}>
              <Grid container alignItems={'center'} justifyContent={'center'}>
                <Grid item xs={1} sm={1} md={1} lg={1} xl={1} >
                  {(isFav) 
                  ? <StarIcon sx={{ color: 'var(--main-bg-color1)' }} /> 
                  : <StarBorderIcon sx={{ color: 'var(--main-bg-color1)' }} />}
                </Grid>
                <Grid item xs={11} sm={11} md={11} lg={11} xl={11}>
                  <TaTypography text={fnFormatNumb(currentPrice)} fontSize={"12pt"} color={'var(--main-bg-color1)'} fontFamily="ArchivoBold" />
                </Grid>
              </Grid>
            </Paper>
            <Paper elevation={0} square={true} classes={{ root: content }}>
              <Grid container alignItems={'center'} justifyContent={'center'}>
                <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align={'left'}>
                  <TaTypography text={itemName} fontSize={"11pt"} color={'var(--main-bg-color1)'} />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align={'left'}>
                  <TaTypography text={descrip2} fontSize={"11pt"} color={'var(--main-bg-color1)'} />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align={'left'}>
                  <TaTypography text={descrip3} fontSize={"11pt"} color={'var(--main-bg-color1)'} />
                </Grid>
              </Grid>
            </Paper>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
            <Paper elevation={0} square={true} classes={{ root: footer }} sx={{ height: '20px', padding: '5px' }}>
              <Grid container alignItems={'center'} justifyContent={'center'}>
                <Grid item xs={7} sm={7} md={7} lg={7} xl={7}>
                  <TaTypography color={'var(--main-text-color1)'} fontSize={'12pt'} text={itemId} fontFamily="ArchivoBold" />
                </Grid>
                <Grid item xs={3} sm={3} md={3} lg={3} xl={3}>
                  <TaTypography color={'var(--main-text-color1)'} fontSize={'12pt'} text={iSpot} fontFamily="ArchivoBold" />
                </Grid>
                <Grid item xs={2} sm={2} md={2} lg={2} xl={2} align={'right'}>
                  <TaTypography color={'var(--main-text-color1)'} fontSize={'12pt'} text={(isUnit === 0) ? existU : exist} fontFamily="ArchivoBold" />
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      </Paper>
      : null
    }
  </ListItemButton>)
})

export default GridItemDetail