import { Grid, ListItemButton, Paper } from '@mui/material'
import TaTypography from 'components/typography/TaTypography'
import useWindowSize from 'helper/UseWindowsSize'

const GridPaymentMethodsDetail = ({ classes: { layout, content, footer },
    paymentMethodData, onClick, onKeyDown, showImage = false }) => {

    const { id, name, urlImage, urlImage2, idCurrency, uniqueKey } = paymentMethodData
    const { height } = useWindowSize()

    return (
        <ListItemButton
            id={`GridItem:${uniqueKey}`}
            sx={{ padding: '0px' }}
            onClick={() => onClick(paymentMethodData)}
            onKeyDown={(e) => onKeyDown(e, paymentMethodData)}>
            <Paper elevation={0} square={true} classes={{ root: layout }}>
                <Grid container alignItems={'flex-start'}>
                    {showImage && <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Paper elevation={0} square={true} classes={{ root: content }}>
                            <img src={urlImage !== '' ? urlImage : urlImage2} alt={''} />
                        </Paper>
                    </Grid>}
                    <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
                        <Paper elevation={0} square={true} classes={{ root: footer }}>
                            <Grid container direction={'row'} justifyContent={'center'} alignItems={'center'}>
                                <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align={'center'} sx={{ height: height - 555 }}>
                                    <TaTypography color={'var(--main-text-color1)'} fontSize={'12pt'} text={name} />
                                </Grid>
                            </Grid>
                        </Paper>
                    </Grid>
                </Grid>
            </Paper>
        </ListItemButton>
    )
}

export default GridPaymentMethodsDetail