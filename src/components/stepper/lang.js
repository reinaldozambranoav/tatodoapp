export default {
    stepperBackButtonLabel: {
        ES: "Atras",
        EN: "Back",
        CN: "之前",
    },
    stepperNextButtonLabel: {
        ES: "Siguiente",
        EN: "Next",
        CN: "下一個",
    },
}