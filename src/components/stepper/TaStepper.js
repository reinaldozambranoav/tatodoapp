import { memo, useState, forwardRef, useImperativeHandle } from 'react'
import { Grid, MobileStepper, Step, StepLabel, Stepper } from '@mui/material'
import { fnMaxIndex } from 'helper/MaxIndex'
import { fnMinIndex } from 'helper/MinIndex'
import useWindowSize from 'helper/UseWindowsSize'
import TaFab from 'components/fab/TaFab'
import TaTypography from 'components/typography/TaTypography'
import PropTypes from 'prop-types'
import { makeStyles } from '@mui/styles'

const stepperStyles = makeStyles({
  listBox: {
    paddingLeft: '20px',
    paddingRight: '5px',
    backgroundColor: props => props.backgroundColor !== undefined ? props.backgroundColor : 'transparent',
    width: props => props.width,
  }
})

const TaStepper = memo(forwardRef(({
  isMobile, labelArray = [], fabColor, btnDisabled,
  children, btnNextHidden, btnBackHidden, onlyMobile = false,
  spacing, hiddenLabel = false, backgroundColor
}, ref) => {

  const { width } = useWindowSize()
  const classes = stepperStyles({ width, backgroundColor })
  const [activeStep, setActiveStep] = useState(0)
  const fnOnBackStep = () => setActiveStep(fnMinIndex(0, activeStep))
  const fnOnNextStep = () => setActiveStep(fnMaxIndex(labelArray.length - 1, activeStep))
  const isHiddenLabel = (isMobile) && !(labelArray.length < 2) && (!hiddenLabel)
  const isHiddenStepper = (!isMobile) && !(labelArray.length < 2) && (!onlyMobile)

  useImperativeHandle(ref, () => ({
    nextStep: fnOnNextStep,
    setStep: setActiveStep //para pasarle el indice que reciba del api de configuration
  }), [labelArray, activeStep])

  return (
    <Grid container spacing={spacing} >
      {isHiddenLabel && <Grid item xs={12} sm={12} md={12} lg={12} xl={12} align={'center'}>
        <TaTypography id={`activeStepLbl${activeStep}`} text={labelArray[activeStep].name} align={'center'} color={'var(--main-text-color0)'} />
      </Grid>}
      {isHiddenStepper && <>
        <Grid item xs={1} sm={1} md={1} lg={1} xl={1} align={'left'}>
          <TaFab
            id={'btnBackStep'}
            color={fabColor ?? 'orange'}
            icon={'backArrow'}
            onClick={fnOnBackStep}
            hidden={btnBackHidden}
            disabled={activeStep === 0} />
        </Grid>
        <Grid item xs={10} sm={10} md={10} lg={10} xl={10} >
          <Stepper activeStep={activeStep} alternativeLabel >
            {labelArray.map((label) => <Step key={label.id}><StepLabel ><TaTypography text={label.name} align={'center'} color={'var(--main-text-color0)'} /></StepLabel></Step>)}
          </Stepper>
        </Grid>
        <Grid item xs={1} sm={1} md={1} lg={1} xl={1} align={'right'}>
          <TaFab
            id={'btnNextStep'}
            color={fabColor ?? 'orange'}
            icon={'forwardArrow'}
            onClick={fnOnNextStep}
            disabled={btnDisabled[activeStep] ?? false}
            hidden={btnNextHidden || activeStep === labelArray.length - 1} />
        </Grid>
      </>}
      {onlyMobile && !isMobile ? children
        : <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
          {children.filter(x => typeof x === 'object')[activeStep]}
        </Grid>}
      {(isMobile) && <MobileStepper
        steps={labelArray.length}
        classes={{ root: classes.root }}
        position={'static'}
        variant={'dots'}
        activeStep={activeStep}
        nextButton={<TaFab
          id={'btnNextStep'}
          color={fabColor ?? 'orange'}
          icon={'forwardArrow'}
          onClick={fnOnNextStep}
          hidden={btnNextHidden}
          disabled={activeStep === labelArray.length - 1 || (btnDisabled[activeStep] ?? false)} />}
        backButton={<TaFab
          id={'btnBackStep'}
          color={fabColor ?? 'orange'}
          icon={'backArrow'}
          onClick={fnOnBackStep}
          hidden={btnBackHidden}
          disabled={activeStep === 0} />}
      />}
    </Grid>
  )
}))

TaStepper.defaultProps = {
  isMobile: false,
  btnDisabled: false,
  btnNextHidden: false,
  btnBackHidden: false,
  spacing: 2
};

export default TaStepper
