import { memo, useState } from 'react'
import { SpeedDial, SpeedDialAction } from '@mui/material'
import { makeStyles } from '@mui/styles'
import PropTypes from 'prop-types'
import { iconType } from 'components/fab/Icons'

const speedDialStyles = makeStyles({
  root: {
    position: 'relative',
    zIndex: 1004,
    height: '40px',
  },
  fab: {
    minHeight: '40px',
    backgroundColor: props => props.color,
    color: 'var(--main-bg-color0)',
    '&:hover': {
      backgroundColor: "var(--main-bg-color3)"
    }
  }
})

const TaSpeedDial = memo(({ id, disabled, actions, direction, color, icon, onClick }) => {

  const [isOpen, setIsOpen] = useState(false)

  const classes = speedDialStyles({ color })
  const speedDialIcon = iconType(icon)

  const fnOpen = () => setIsOpen(preVal => !preVal)

  const onClickAction = (id, uniqueKey) => onClick(id, uniqueKey)

  return (
    <SpeedDial
      id={id}
      ariaLabel={'speedDial'}
      hidden={false}
      icon={speedDialIcon}
      direction={direction}
      open={isOpen}
      onOpen={fnOpen}
      onClose={fnOpen}
      classes={{ root: classes.root, fab: classes.fab }}
      FabProps={{
        disabled: disabled,
        size: 'small',
      }} >
      {actions.map((x) => {
        let actionsClasses = speedDialStyles({ color: x.color })
        let actionsIcon = iconType(x.icon)
        return (<SpeedDialAction
          key={x.uniqueKey}
          icon={actionsIcon}
          tooltipTitle={x.name}
          FabProps={{
            classes: { root: actionsClasses.fab },
            size: 'small',
            disabled: x.disabled
          }}
          onClick={() => onClickAction(x.id, x.uniqueKey)}
        />)
      })}
    </SpeedDial>
  )
})

TaSpeedDial.propTypes = {
  id: PropTypes.string,
  hidden: PropTypes.bool,
  icon: PropTypes.string.isRequired,
  actions: PropTypes.array.isRequired,
  direction: PropTypes.string,
  color: PropTypes.string,
  onClick: PropTypes.func.isRequired,
}

TaSpeedDial.defaultProps = {
  hidden: false,
  actions: [],
  direction: 'down',
  color: 'var(--main-bg-color4)',
  icon: ''
}

export default TaSpeedDial