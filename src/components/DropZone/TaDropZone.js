import { memo, useMemo, useState, Fragment } from 'react'
import { useDropzone } from 'react-dropzone'
import { Grid } from '@mui/material'
import TaTypography from '../typography/TaTypography'
import TaList from 'components/list/TaList'
import TaListItem from 'components/list/TaListItem'
import generalLanguages from 'lang/Lang'
import * as XLSX from 'xlsx/xlsx.mjs'
import fnGetTypeOfElement from 'helper/fnGetTypeOfElement'
import TaImg from 'components/img/TaImg'

const baseStyle = {
  flex: 1,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  padding: '5px',
  borderWidth: 2,
  borderRadius: 2,
  borderColor: '#eeeeee',
  borderStyle: 'dashed',
  backgroundColor: '#fafafa',
  color: '#bdbdbd',
  outline: 'none',
  transition: 'border .24s ease-in-out',
  margin: '7px 0px 0px 0px'
}

const activeStyle = {
  borderColor: '#2196f3'
}

const acceptStyle = {
  borderColor: '#00e676'
}

const rejectStyle = {
  borderColor: '#ff1744'
}

const imageMaxSize = 1000000000 // bytes
const { dropZoneLang: { dropAreaLang, dropSquareLang, dropZoneErrorsOutputLang: { header, excelSheet, value, column, line, errorType } } } = generalLanguages

const TaDropZone = memo(({ formatType, disabled = false, languageState = 'ES', onCallback, validateColumns = [], strictDataTypes = [] }) => {
  const isImage = formatType === '.jpg'
  const [files, setFile] = useState(undefined)
  const [isReady, setIsReady] = useState(false)
  const [error, setError] = useState(false)
  const reader = new FileReader()

  const fnFileResult = (acceptedFiles = []) => {
    if (acceptedFiles.length > 0) {
      setError(false)
      setIsReady(false)
      return new Promise(async (resolve, reject) => {
        try {
          switch (acceptedFiles[0].type) {
            case 'image/jpeg':
            case 'image/png':
              reader.onload = (e) => {
                const binaryStr = reader.result
                // console.log('binaryStr', binaryStr)
                resolve(binaryStr)
              }
              reader.readAsDataURL(acceptedFiles[0])
              break
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            case 'xlsx':
            case 'ods':
              let newObj, errors = [`${header[languageState]}`, '\r\n']
              const sheetToJson = (data) => XLSX.utils.sheet_to_json(data,)

              reader.onload = (e) => {
                const rawData = e.target.result
                const workbook = XLSX.read(rawData, { type: 'array' })
                const sheets = workbook.Sheets
                for (const key in sheets) newObj = ({ ...newObj, [key]: sheetToJson(sheets[key]) })
                for (const key in newObj) {
                  newObj = ({
                    ...newObj, [key]: newObj[key].map((x, i) => {
                      let newObj
                      for (const property in x) newObj = ({ ...newObj, [property.split('*')[0]]: x[property], index: i })
                      return newObj
                    })
                  })
                  newObj[key].forEach((x, i) => {
                    for (const subKey in x) {
                      if (validateColumns.length > 0 && validateColumns.includes(subKey)) {
                        if (!strictDataTypes.includes(fnGetTypeOfElement(x[subKey]))) {
                          if (!errors.includes(`${excelSheet[languageState]} (${key})`)) {
                            errors = [...errors, '\r\n']
                            errors = [...errors, `${excelSheet[languageState]} (${key})`, '\r\n']
                          }
                          setError(true)
                          errors = [...errors, `${value[languageState]} (${x[subKey]}) ${column[languageState]} (${subKey}) ${line[languageState]} (${(i + 2)}) ${errorType[languageState]} `, '\r\n']
                        }
                      }
                    }
                  })
                }
                if (errors.length > 2) reject(errors)
                resolve(newObj)
              }
              reader.readAsArrayBuffer(acceptedFiles[0])
              break
            default:
              break
          }
          setIsReady(true)
        } catch (error) {
          console.error('error de promesa', error)
          reject(error)
        }
      })
    }
  }

  const fnOnDrop = async (file) => {
    // console.log(file)
    try {
      const result = await fnFileResult(file)
      // console.log(result)
      setFile(result)
      if (result !== undefined) onCallback(result)
    } catch (error) {
      console.error('error en fnOnDrop', error)
      onCallback(error)
    }
  }

  const fnOnClick = (event) => {
    try {
      setError(false)
      setIsReady(false)
      getRootProps().onClick(event)
    } catch (error) {
      console.error('fnOnClick => ', error)
    }
  }

  const { getRootProps, getInputProps, isDragActive, isDragAccept, isDragReject, acceptedFiles } =
    useDropzone({ onDrop: fnOnDrop, accept: formatType, maxFiles: 1 })

  const style = useMemo(() => ({
    ...baseStyle,
    ...(isDragActive ? activeStyle : {}),
    ...(isDragAccept || isReady === true ? acceptStyle : {}),
    ...(isDragReject || error ? rejectStyle : {})
  }), [isDragActive, isDragReject, isDragAccept, isReady, error])

  return (
    <div {...getRootProps({ style })} onClick={disabled ? undefined : fnOnClick} onDrop={disabled ? undefined : getRootProps().onDrop} >
      <input {...getInputProps()} />
      {isReady
        ? acceptedFiles && <TaList elevation={0} >
          {acceptedFiles && acceptedFiles.map(x => {
            return (
              <Fragment key={`File => ${x.name}`}>
                {!isImage && <TaListItem >
                  <Grid container style={{ margin: '5px' }} alignItems='center' alignContent={'flex-start'}>
                    <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>{`${x.name}`}</Grid>
                  </Grid>
                </TaListItem>}
                {
                  // isImage && <img src={files} height={'120px'} />
                  isImage && <TaImg url={files} inHeight={'120px'} />
                }
              </Fragment>
            )
          })}
        </TaList>
        : isDragActive
          ? <TaTypography align='center'>{dropAreaLang[languageState]}</TaTypography>
          : <TaTypography align='center'>{dropSquareLang[languageState]} </TaTypography>}
    </div>
  )
})

export default TaDropZone