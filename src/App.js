import { Grid } from "@mui/material"
import TaTextField from "components/textfield/TaTextField"

const App = () => {
  return (
    <Grid container >
      <Grid item xs={12} sm={12} md={12} lg={12} xl={12}>
        <TaTextField label={'Hello world'} />
      </Grid>
    </Grid>
  )
}

export default App
