export const fnPreventDefaultSubmit = (e) => (e.key === 'Enter') && e.preventDefault()

export default fnPreventDefaultSubmit