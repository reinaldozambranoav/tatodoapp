export const fnBuildFormFromArray = (data, model = undefined) => {
  try {
    let newObj
    data.forEach(x => {
      for (const key in x) {
        newObj = {
          ...newObj,
          [ x.uniqueKey ]: (model !== undefined) ? model(x) : { ...x, [ key ]: x[ key ] }
        }
      }
    })
    return newObj
  } catch (error) {
    console.error('fnBuildFormFromArray => ', error)
  }
}

export const fnRebuldFormDataIntoArray = (data) => {
  try {
    let newArray = []
    for (const key in data) {
      if (typeof data[ key ] === 'object') {
        newArray = [ ...newArray, data[ key ] ]
      }
    }
    return newArray
  } catch (error) {
    console.error('fnRebuildFormDataIntoArray => ', error)
  }
}