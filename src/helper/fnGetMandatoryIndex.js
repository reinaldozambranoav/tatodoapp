const fnGetMandatoryIndex = (array, uniqueKey, keyName, propertyName) => `${keyName}.${array.findIndex(x => x.uniqueKey === uniqueKey)}.${propertyName}`
export default fnGetMandatoryIndex