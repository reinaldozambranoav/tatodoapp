export const TaSearch = (data = [], filter = '', generic = false, isSaintSearch = false) => {

  let newData = [...data]

  const lastIndex = newData.length - 1

  if (filter !== '') {
    const newFilter = String(filter.toLowerCase()).replace(/\*/g, ".*")
    const newMatch = isSaintSearch === false ? new RegExp('^.*' + newFilter + '.*$') : new RegExp('^' + newFilter + '.*$')

    if (generic === false) {
      return newData.filter(x => ((newMatch.test(String(x.id).toLowerCase())) ||
        (newMatch.test(String(x.name + (x.descrip2 !== undefined ? x.descrip2 : '') + (x.descrip3 !== undefined ? x.descrip3 : '')).toLowerCase()))
        ||
        (newMatch.test(String(x.refere !== undefined ? String(x.refere) : '').toLowerCase())) && x.active === 1).slice(0, lastIndex))
    } else {
      return newData.filter(x => ((newMatch.test(String(x.id).toLowerCase()))
        ||
        (newMatch.test(String(x.name).toLowerCase())))).slice(0, lastIndex)
    }

  } else {
    return newData
  }

}