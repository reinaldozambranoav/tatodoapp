export const TaIsEmpty = (value) => !(value !== null && value !== '' && value !== undefined)
export const TaIsLoader = (value, type = true) => { return (value === undefined ? type : value) }
export const TaBool = (value) => { return (value) ? value === true ? false : true : false }
export const TaIsBool = (value, dvalue = false) => value === undefined ? dvalue : value
export const TaIsIdNullOnList = (value) => value.find(x => x.id === null)