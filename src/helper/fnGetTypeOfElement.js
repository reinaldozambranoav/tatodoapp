const fnGetTypeOfElement = (element) => element === null
    ? 'null'
    : Array.isArray(element)
        ? 'array'
        : Number(element) === NaN
            ? 'NaN'
            : typeof element

export default fnGetTypeOfElement