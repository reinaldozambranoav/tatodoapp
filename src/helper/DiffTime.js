import * as moment from "moment";
export const fnDiffTime = (iniDate, endDate) => {
    const diff = moment.duration(new Date(endDate) - new Date(iniDate))
    return `${diff.hours()}:${diff.minutes()}:${diff.seconds()}`
}