export const sumValues = (data, column, initialValue = 0) =>
    data.length > 0
        ? data.reduce((acu, x) => acu + x[column], initialValue)
        : initialValue