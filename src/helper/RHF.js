export const setDataRHF = (data, setValue) => {
    for (let property in data) {
        setValue(property, data[property])
    }
}

export const validateErrorRHF = (error, key, message) => error ? error[key] ? message ?? error[key]?.message : undefined : undefined