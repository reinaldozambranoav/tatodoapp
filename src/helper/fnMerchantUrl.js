import { LOCAL_STORAGE_CONNECTION } from "constants/LocalStorage"
import { dbSimplit } from "services/localforage/Localforage"

export const fnMerchantUrl = async (currentUrl, stationService) => {
    return currentUrl !== "https://"
    ? currentUrl
    : stationService !== "https://"
        ? stationService
        : await dbSimplit.getItem(LOCAL_STORAGE_CONNECTION)
           
}