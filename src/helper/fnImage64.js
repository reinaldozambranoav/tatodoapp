export const fnImage64toCanvasRef = (canvasRef, image64, pixelCrop) => {

  const canvas = canvasRef // document.createElement('canvas');
  // console.log('pixelCrop', pixelCrop)
  // console.log('canvas1', canvas)
  // console.log('image64', image64)

  canvas.width = pixelCrop.width
  canvas.height = pixelCrop.height
  const ctx = canvas.getContext('2d')
  // console.log('ctx =>', ctx)
  const image = new Image()
  // console.log('aaaaaimage', image)
  image.src = image64
  // console.log(image, pixelCrop.x, pixelCrop.y, pixelCrop.width, pixelCrop.height, 0, 0, pixelCrop.width, pixelCrop.height)
  image.onload = () => {
    ctx.drawImage(
      image,
      pixelCrop.x,
      pixelCrop.y,
      pixelCrop.width,
      pixelCrop.height,
      pixelCrop.x,
      pixelCrop.y,
      pixelCrop.width,
      pixelCrop.height
    )
  }

  // console.log('canvas2', canvas)
}

export const fnUrlToBase64 = (url) => {
  return new Promise((resolve, reject) => {
    try {
      var xhr = new XMLHttpRequest()
      xhr.open("GET", url, true)
      xhr.responseType = "blob"
      xhr.onload = function (e) {
        var reader = new FileReader()
        reader.onload = function (event) {
          resolve(event.target.result)
        }
        var file = this.response
        reader.readAsDataURL(file)
      }
      xhr.send()
    } catch (error) {
      console.error('fnUrlToBase64 => ', error)
      reject(error)
    }
  })
}
